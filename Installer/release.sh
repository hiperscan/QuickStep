#!/bin/bash

set -e

CHANGELOG=../GUI/CHANGELOG.txt
RECIPENTS=\
rene.rietzschel@hiperscan.com,\
alexander.wolter@hiperscan.com,\
marc.althaus@hiperscan.com,\
torsten.schache@hiperscan.com,\
sebastian.kowal@hiperscan.com,\
kathrin.trabs@hiperscan.com,\
silke.lenhardt@hiperscan.com,\
ingo.heinze@hiperscan.com,\
thomas.klose@hiperscan.com,\
florian.stuelpner@hiperscan.com,\
sebastian.kleinschmidt@hiperscan.com,\
maria.kossack@hiperscan.com,\
laura.meyer@hiperscan.com,\
julia.hollmach@hiperscan.com,\
henrik.schondorff@hiperscan.com,\
marco.richter@hiperscan.com,\
karl-heinz.boehm@hiperscan.com,\
herbert.c.hoffmann@hiperscan.com,\
franziska.brocke@hiperscan.com,\
antoine.mathey@hiperscan.com,\
alexander.hoepfner@hiperscan.com,\
walter.kochte@hiperscan.com,\
benjamin.wittek@hiperscan.com,\
toni.lichtlein@hiperscan.com,\
michelle.boehme@hiperscan.com

make clean
make all

MSIFILE=$(ls QuickStep*.exe)
DEBFILE=$(ls quickstep*.deb)
BASEDIR=software/releases/quickstep/development
BASEURL=https://webdav.hiperscan.com/${BASEDIR}/$MSIFILE

for date in `egrep -n '^[0-9]+' $CHANGELOG`
do
        LASTDATE=$date
done

if [ ! -e /media/software ]
then
  echo "/media/software is not mounted."
  exit 1
fi

mv $MSIFILE /media/${BASEDIR}
mv $DEBFILE /media/${BASEDIR}
chmod 755 /media/${BASEDIR}/$MSIFILE

STARTLINE=`echo $LASTDATE | sed -e 's/:.*$//'`
TOTALLINE=`cat $CHANGELOG | wc -l`
LINES=$(($TOTALLINE - $STARTLINE + 2))

TEXT="Die aktuelle Entwicklerversion von QuickStep ist unter folgender URL verfügbar:"
TEXT=${TEXT}`echo -e "\n\n${BASEURL}${NEWFILE}"`

TEXT=${TEXT}`echo -e "\n\n\nLetzter Eintrag des Changelogs: "`
TEXT=${TEXT}`tail -n $LINES $CHANGELOG | sed -e 's/^\*/  \*/g'`
TEXT=${TEXT}`cat mail.template`

evolution mailto:${RECIPENTS}?subject="[quickstep] Neue Version verfügbar"\&body="${TEXT}"

