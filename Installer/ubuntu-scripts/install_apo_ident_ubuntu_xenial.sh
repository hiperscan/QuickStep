#!/bin/bash

sudo apt-get install wget apt-transport-https

wget -q https://Apo-Ident:Apo-Ident@www.hiperscan-download.de/software/public/repository/hiperscan.asc -O - | sudo apt-key add -

grep hiperscan-download.de /etc/apt/sources.list > /dev/null
if [ $? == "1" ]
then
  sudo sh -c 'sudo echo deb https://Apo-Ident:Apo-Ident@hiperscan-download.de/software/public/repository xenial contrib >> /etc/apt/sources.list'
else
  sudo perl -p -i -e 's/deb https:\/\/Apo-Ident/#deb https:\/\/Apo-Ident/' /etc/apt/sources.list
  sudo sh -c 'sudo echo deb https://Apo-Ident:Apo-Ident@hiperscan-download.de/software/public/repository xenial contrib >> /etc/apt/sources.list'
fi

sudo apt-get update
sudo apt-get -y install quickstep identserver
