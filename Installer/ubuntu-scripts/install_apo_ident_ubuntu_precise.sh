#!/bin/bash

sudo apt-get install wget apt-transport-https

wget -q https://Apo-Ident:Apo-Ident@www.hiperscan-download.de/software/public/repository/hiperscan.asc -O - | sudo apt-key add -

grep hiperscan-download.de /etc/apt/sources.list > /dev/null
if [ $? == "1" ]
then
  sudo sh -c 'sudo echo deb https://Apo-Ident:Apo-Ident@hiperscan-download.de/software/public/repository precise contrib >> /etc/apt/sources.list'
fi

sudo apt-get update
sudo apt-get -y install quickstep identserver

if [ ! -f /usr/lib/cups/filter/rastertoptch ]
then
    if [ $(uname -m) == "x86_64" ]
    then
        sudo cp /usr/lib/x86_64-linux-gnu/cups/filter/rastertoptch /usr/lib/cups/filter/
    else
        sudo cp /usr/lib/i386-linux-gnu/cups/filter/rastertoptch /usr/lib/cups/filter/
    fi
fi

