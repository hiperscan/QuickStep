#!/bin/bash
# quickstep - start script
# User: klose at 19:54 25.10.2009
# QuickStep: Acquire, view and identify spectra 
# Copyright (C) 2009-2010  Thomas Klose, Hiperscan GmbH Dresden, Germany

MONO=/usr/bin/mono
MONO_VERSION=$($MONO -V |grep version| sed 's/.* \([0-9]*.[0-9]*\).[0-9]* .*/\1/')
OPTIONS=
FILES=
THUMBNAILER=0
MONO_OPTIONS="--debug"
XVFB=/usr/bin/xvfb-run

for arg in "$@"
do
  if [[ $arg == "-t" || $arg == "--thumbnailer" ]]
  then
    eval $MONO ./QuickStep.GUI.exe "$@"
    exit $?
  fi
  if [[ $arg == "-h" || $arg == "--hide-gui" ]]
  then
    if [ ! -f $XVFB ]
    then
      echo "Please install package xvfb to run quickstep in batch mode"
      exit 1
    fi
     MONO="$XVFB -a $MONO"
  fi
  echo $arg | grep -q -P '.*\.[cC][sS][vV][cC]{0,1}'
  if [[ $? = 1 || $THUMBNAILER = 1 ]]
  then
    OPTIONS="$OPTIONS $arg"
  else
    FILES="$FILES '`readlink -f \"$arg\"`'"
  fi
done

cd /usr/lib/quickstep

export LIBXCB_ALLOW_SLOPPY_LOCK=1
export LD_LIBRARY_PATH=./:$LD_LIBRARY_PATH

if [ -d /usr/lib/hiperscan-gtk-sharp2 ]
then
  export MONO_GAC_PREFIX=/usr/lib/hiperscan-gtk-sharp2
  export LD_LIBRARY_PATH=/usr/lib/hiperscan-gtk-sharp2/lib:$LD_LIBRARY_PATH
fi

if [ -f /usr/lib/hiperscan-libgdiplus/etc/config ]
then
  MONO_OPTIONS="--config /usr/lib/hiperscan-libgdiplus/etc/config $MONO_OPTIONS"
  export LD_LIBRARY_PATH=/usr/lib/hiperscan-libgdiplus/lib:$LD_LIBRARY_PATH
fi

if [ "$($MONO $MONO_OPTIONS ./QuickStep.GUI.exe --i18n)" != "True" ]
then
  export LANGUAGE=en
  export LANG=en_GB.utf-8
  export LC_ALL=en_GB.utf-8
fi

EASTER_DATE=$(LANG=C ncal -e)
EASTER_MIN=$(date -d "$EASTER_DATE -7 days" +%s)
EASTER_MAX=$(date -d "$EASTER_DATE +1 days" +%s)
XMAS_DATE=$(date -d "`date +%y`-12-24" +%y-%m-%d)
XMAS_MIN=$(date -d "$XMAS_DATE -14 days" +%s)
XMAS_MAX=$(date -d "$XMAS_DATE +1 days" +%s)
NOW_DATE=$(date +%s)

if [ $NOW_DATE -gt $XMAS_MIN ] && [ $NOW_DATE -lt $XMAS_MAX ]
then
  OPTIONS="$OPTIONS --icon-type Christmas"
fi

if [ $NOW_DATE -gt $EASTER_MIN ] && [ $NOW_DATE -lt $EASTER_MAX ]
then
  OPTIONS="$OPTIONS --icon-type Easter"
fi

export MONO_GC_PARAMS=nursery-size=128m

mkdir -p ~/.QuickStep
eval $MONO $MONO_OPTIONS ./QuickStep.GUI.exe $OPTIONS $FILES 2>&1 | grep --line-buffered -v "libglade-WARNING.*unknown attribute.*swapped" | grep --line-buffered -v "^$" | tee -a /var/log/quickstep/quickstep.log
