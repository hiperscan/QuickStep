
#!/bin/bash
# calibration - start script
# User: schache at 26.09.2014
# Calibration: Configure and creates pls models
# Copyright (C) 2009-2014  Hiperscan GmbH Dresden, Germany

quickstep > /dev/null &
sleep 5
quickstep -r calibration -c calibration
