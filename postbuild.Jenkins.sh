#!/bin/sh

set -e

CDIR=$(pwd)

if [ -z $1 ]
then
  echo "Cannot run postbuild scripts without a Profile."
  exit 1
fi

for SCRIPT in $(find .. -type f -name 'postbuild.sh')
do
  echo "Running postbuild script $SCRIPT for profile $1..."
  TARGETDIR=$(dirname $SCRIPT)/bin/$1
  if ! [ -d $TARGETDIR ]
  then
    continue
  fi
  cd $TARGETDIR
  bash ../../postbuild.sh
  cd $CDIR
done


