PROFILE=Debug
MONO_BUNDLE_OPTIONS=--debug

# From Mono documentation regarding NETVERSION:
# Note: The 4.5 folder contains the actual assemblies that are used during runtime. We’re considering 
# these the latest assemblies from now on, i.e. right now they’re implementing .NET 4.6.1. 
# Unfortunately, we couldn’t rename this folder as too many apps and libraries hardcoded checks for 
# this path.

SUFFIX:=cs
CULTURE:=de-de
MONOVERSION:=6.8.0.105
NETVERSION:=4.5
MONOVCRVERSION:=14.24
GTKSHARPVERSION:=1.12.45
GTKVCRVERSION:=12.0
LIBMONO=mono-2.0-sgen

BUILDDEPS:=/media/software/builddeps
BINDIR:=../GUI/bin/$(PROFILE)

ASMFILE:=$(BINDIR)/QuickStep.GUI.exe
ASMCONF:=$(BINDIR)/QuickStep.GUI.exe.config
ASMINFO:=../GUI/Properties/AssemblyInfo.generated.cs

ASMSIZE=$(shell du -b $(ASMFILE)|cut -f1)
VERSION=$(shell grep AssemblyVersion $(ASMINFO) | sed -e 's/.*(\"//' -e 's/\").*//' -e 's/\.\*//')
VERSION_FULL=$(wordlist 1,3,$(subst ., ,$(VERSION)))
VERSION_MAJOR=$(word 1,$(VERSION_FULL))
VERSION_MINOR=$(word 2,$(VERSION_FULL))
VERSION_PATCH=$(word 3,$(VERSION_FULL))
LANGUAGE=$(wordlist 1,1,$(subst -, ,$(CULTURE)))

MONODIR=$(BUILDDEPS)/Mono/$(MONOVERSION)
GTKSHARPDIR=$(BUILDDEPS)/GtkSharp/$(GTKSHARPVERSION)
MONOVCRDIR=$(BUILDDEPS)/VisualC++/$(MONOVCRVERSION)
GTKVCRDIR=$(BUILDDEPS)/VisualC++/$(GTKVCRVERSION)
VCRFMASK:=vc*redist*x86.exe

ifeq ($(SUFFIX),os)
	PLUGINS:=Acquisition AdditionalPlotTypes ApoIdent UserDefinedPlotType OnlineProcessing FinderSelftest
	APPLICATIONS:=quickstep-apo-ident.desktop quickstep.desktop
	UPDATECERTIFICATELAUNCHER:=../../ApoIdent/UpdateCertificate/UpdateCertificateStarter/Launcher/UpdateCertificateLauncher.exe
	PHARMAIDENTSERVERLAUNCHER:=../../PharmaIdentcs/PharmaIdentServer/Launcher/PharmaIdentServer.exe
endif

# this options are needed for Mono versions >5.10
DEFINE_ASM_NAMES=yes
USE_SYSTEM_CONFIG=yes

DEFINE_RELEASE_CANDIDATE=no
RELEASE_CANDIDATE_VERSION=GRAPHIC
