#include <unistd.h>
#include <locale.h>
#include <windows.h>
#include <shlobj.h>
#include <sys/stat.h>
#include <mono/metadata/mono-config.h>
#include <mono/metadata/assembly.h>

#include <mono/jit/jit.h>

#ifdef _DEFINE_ASM_NAMES
#define _ASM(name) asm(name)
#else
#define _ASM(name)
#endif

extern const unsigned char assembly_data_QuickStep_GUI_exe[] _ASM("assembly_data_QuickStep_GUI_exe");
static const MonoBundledAssembly assembly_bundle_QuickStep_GUI_exe = {"QuickStep.GUI.exe", assembly_data_QuickStep_GUI_exe, _ASSEMBLY_SIZE};
extern const unsigned char assembly_config_QuickStep_GUI_exe[] _ASM("assembly_config_QuickStep_GUI_exe");
extern const unsigned char assembly_data_I18N_West_dll[] _ASM("assembly_data_I18N_West_dll");
static const MonoBundledAssembly assembly_bundle_I18N_West_dll = {"I18N.West.dll", assembly_data_I18N_West_dll, 72192};
extern const unsigned char assembly_data_I18N_dll[] _ASM("assembly_data_I18N_dll");
static const MonoBundledAssembly assembly_bundle_I18N_dll = {"I18N.dll", assembly_data_I18N_dll, 38912};

#ifdef _SYSTEM_CONFIG
extern const char system_config _ASM("system_config");
#endif

static const MonoBundledAssembly *bundled[] = {
	&assembly_bundle_QuickStep_GUI_exe,
	&assembly_bundle_I18N_West_dll,
	&assembly_bundle_I18N_dll,
	NULL
};


static void install_aot_modules (void)
{
	mono_jit_set_aot_mode (MONO_AOT_MODE_NORMAL);
}

static char *image_name = "QuickStep.GUI.exe";

static void install_dll_config_files(void)
{
	mono_register_config_for_assembly("QuickStep.GUI.exe", (char*)assembly_config_QuickStep_GUI_exe);

#ifdef _SYSTEM_CONFIG
	mono_config_parse_memory(&system_config);
#endif
}

static const char *config_dir = NULL;
void mono_mkbundle_init()
{
	install_dll_config_files();
	mono_register_bundled_assemblies(bundled);
	install_aot_modules();
}
int mono_main(int argc, char* argv[]);

#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include <windows.h>
#endif

static char **mono_options = NULL;
static int count_mono_options_args(void)
{
	const char *e = getenv("MONO_BUNDLED_OPTIONS");
	const char *p, *q;
	int i, n;

	if (e == NULL)
		return 0;

	/* Don't bother with any quoting here. It is unlikely one would
	 * want to pass options containing spaces anyway.
	 */

	p = e;
	n = 1;
	while ((q = strchr(p, ' ')) != NULL) {
		n++;
		p = q + 1;
	}

	mono_options = malloc(sizeof(char *) * (n + 1));

	p = e;
	i = 0;
	while ((q = strchr(p, ' ')) != NULL) {
		mono_options[i] = malloc((q - p) + 1);
		memcpy(mono_options[i], p, q - p);
		mono_options[i][q - p] = '\0';
		i++;
		p = q + 1;
	}
	mono_options[i++] = strdup(p);
	mono_options[i] = NULL;

	return n;
}

BOOL starts_with(const char *pre, char *str)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? FALSE : strncmp(pre, str, lenpre) == 0;
}


int main(int argc, char* argv[], char* envp[])
{
	BOOL log = FALSE;
	int ix;
	for (ix=0; ix < argc; ++ix)
	{
		if (strcmp(argv[ix], "--log") == 0)
			log = TRUE;
	}

	char logfile_path[MAX_PATH];
	char temp_path[MAX_PATH];
	if (SUCCEEDED(GetTempPath(MAX_PATH, temp_path)))
	{
		sprintf(logfile_path, "%s\\quickstep_log.txt", temp_path);
		struct stat filestatus;
		stat(logfile_path, &filestatus);
		if (filestatus.st_size > 500000)
			remove(logfile_path);
	}
	else
	{
		log = FALSE;
	}

	if (log)
		*stdout = *fopen(logfile_path, "a");

	if (stdout != NULL)
	{
		printf("Running Windows launcher ...\n");
		printf("Current Environment variables:\n");
		char** env;
		for (env = envp; *env != 0; env++)
		{
			printf("\t%s\n", *env);
		}
	}
	else
	{
		log = FALSE;
	}

	char **newargs;
	int i, k = 0;

	newargs = (char **)malloc(sizeof(char*) * (argc + 2 + count_mono_options_args()));

	newargs[k++] = argv [0];

	if (mono_options != NULL)
	{
		i = 0;
		while (mono_options[i] != NULL)
			newargs[k++] = mono_options[i++];
	}

	newargs[k++] = image_name;

	for (i = 1; i < argc; i++)
	{
		newargs[k++] = argv[i];
	}
	newargs[k] = NULL;
	
	if (config_dir != NULL && getenv("MONO_CFG_DIR") == NULL)
		mono_set_dirs(getenv("MONO_PATH"), config_dir);

	// executable's directory
	char cwddrive[10];
	char cwdpath[1024];
	if (_splitpath_s(_pgmptr, cwddrive, sizeof(cwddrive), cwdpath, sizeof(cwdpath), NULL, 0, NULL, 0))
        {
		perror("_splitpath_s() error");	
		return -1;
	}

	char cwd[2048];
	sprintf(cwd, "%s%s", cwddrive, cwdpath);

	// set environment variables
	char mono_path_env[2048];
	char mono_bin_path[2048];
	char gtk_path[2048];
	char hcl_path[2048];
	char gtk_extensions_path[2048];
	char path_env[2048*6];
	char qs_basepath_env[2048];
	char gtk_basepath_env[2048];
	char hcl_path_env[2048];
	char machine_config_path_env[2048];

	sprintf(hcl_path, "%sHCL", cwd);
	sprintf(gtk_extensions_path, "%sGtkExtensions", cwd);
	sprintf(mono_bin_path, "%sMono\\bin", cwd);
	sprintf(gtk_path, "%sGtk\\bin", cwd);

        
        char* windir_path = getenv("windir");
	//char* current_path = getenv("PATH");
        //if (current_path == NULL)
 	sprintf(path_env, "PATH=%s;%s;%s;%s;%s;%s\\System32", cwd, mono_bin_path, gtk_path, hcl_path, gtk_extensions_path, windir_path);
	//else
	//	sprintf(path_env, "PATH=%s;%s;%s;%s;%s;%s", cwd, mono_bin_path, gtk_path, hcl_path, gtk_extensions_path, current_path);

	sprintf(qs_basepath_env, "QS_BASEPATH=%s", cwd);
	sprintf(gtk_basepath_env, "GTK_BASEPATH=%sGtk", cwd);
        sprintf(hcl_path_env, "HCL_PATH=%s", hcl_path);
	sprintf(mono_path_env, "MONO_PATH=%s;%sMono\\lib\\mono\\%s;%sGtk\\lib\\gtk-sharp-2.0;%s", cwd, cwd, _NETVERSION, cwd, hcl_path);
	sprintf(machine_config_path_env, "MONO_CFG_DIR=%sMono\\etc", cwd);

	printf("Setting environment ...\n");
	printf("mono_path_env: %s\n", mono_path_env);
	printf("path_env: %s\n", path_env);
	printf("qs_basepath_env: %s\n", qs_basepath_env);
	printf("gtk_basepath_env: %s\n", gtk_basepath_env);
	printf("hcl_path_env: %s\n", hcl_path_env);
	printf("machine_config_path_env: %s\n", machine_config_path_env);

	putenv(mono_path_env);
	putenv(path_env);
	putenv(qs_basepath_env);
	putenv(gtk_basepath_env);
	putenv(hcl_path_env);
        putenv("MONO_GC_PARAMS=nursery-size=64m");
	putenv(machine_config_path_env);

	printf("Preloading dynamically linked libraries ...\n");
	BOOL success = TRUE;

	char zlib_path[2048];
	sprintf(zlib_path, "%s\\zlib1.dll", gtk_path);
	if (LoadLibrary(zlib_path) == NULL)
	{
		printf("Cannot load %s\n", zlib_path);
		success = FALSE;
	}

	char libgtk_path[2048];
	sprintf(libgtk_path, "%s\\libgtk-win32-2.0-0.dll", gtk_path);
	if (LoadLibrary(libgtk_path) == NULL)
	{
		printf("Cannot load %s\n", libgtk_path);
		success = FALSE;
	}
	
	if (success)
	{
		printf("Success.\n");
	}
	else
	{
		printf("Failed.\n");
	}

	// locale settings
	if (getenv("LANG") == NULL)
	{
		LCID lcid = GetSystemDefaultLCID() & 0xFF;

		switch (lcid)
		{

		case 0x07: // German
			printf("Setting language: LANG=de\n");
			putenv("LANG=de");
			break;
            /* Czech is deactivated
		case 0x05: // Czech
			printf("language: LANG=cs\n");
			putenv("LANG=cs");
			break;*/

		default: // English
			printf("Setting language: LANG=en\n");
			putenv("LANG=en");
			break;

		}
	}
	else
	{
		if (starts_with("de", getenv("LANG")) == TRUE)
		{
			printf("Setting language: LANG=de\n");
			putenv("LANG=de");
		}
        /* Czech is deactivated
		else if (starts_with("cs", getenv("LANG")) == TRUE)
		{
			printf("language: LANG=cs\n");
			putenv("LANG=cs");
		}*/
		else
		{
			printf("Setting language: LANG=en\n");
			putenv("LANG=en");
		}
	}

	unsigned int codepage = GetACP();
	printf("Current Codepage: %i\n", codepage);

	switch (codepage)
	{

	case 1252: // OEM Multilingual Latin 1; Western European (DOS)
		putenv("MONO_EXTERNAL_ENCODINGS=latin1:utf8");
		printf("Setting MONO_EXTERNAL_ENCODINGS=latin1:utf8\n");
		break;

	default:
		break;

	}	

	// prevent standby (not supported by usb driver), set awaymode
	SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED);
	
	mono_mkbundle_init();

	printf("Running Mono bundle...\n");

        for (ix=0; ix < k; ++ix)
	{
		printf("argument %i: %s\n", ix, newargs[ix]);
	}

	if (log)
	{
		fflush(stdout);
		fclose(stdout);
	}

	return mono_main(k, newargs);
}

