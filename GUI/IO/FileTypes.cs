﻿// FileTypes.cs created with MonoDevelop
// User: klose at 13:56 04.01.2011
// CVS release: $Id: FileTypes.cs,v 1.2 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System.Collections.Generic;
using System.Globalization;

using Hiperscan.Spectroscopy;
using Hiperscan.Spectroscopy.IO;


namespace Hiperscan.QuickStep.IO
{

    public class CsvFileTypeInfo : Hiperscan.Extensions.FileTypeInfo
    {
        public CsvFileTypeInfo() : base("QuickStep (csv)", new string[] {"csv"})
        {
        }
        
        public override Spectrum Read(string fname)
        {
            return CSV.Read(fname);
        }
        
        public override void Write(Spectrum spectrum, string fname, CultureInfo culture)
        {
            CSV.Write(spectrum, fname, culture);
        }
    }
    
    public class CsvContainerFileTypeInfo : Hiperscan.Extensions.FileTypeInfo
    {
        public CsvContainerFileTypeInfo() : base("QuickStep container (csvc)", new string[] {"csvc"})
        {
            this.is_container = true;
        }
        
        public override Spectrum[] ReadContainer(string fname, out List<MetaData> meta_data)
        {
            return CSV.ReadContainer(fname, out meta_data);
        }
        
        public override void WriteContainer(List<Spectrum> spectra, List<MetaData> meta_data, string fname, CultureInfo culture)
        {
            CSV.WriteContainer(spectra, meta_data, fname, culture);
        }
    }
}