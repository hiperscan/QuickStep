﻿// DirectoryProperty.cs created with MonoDevelop
// User: klose at 13:41 16.02.2009
// CVS release: $Id: DirectoryProperty.cs,v 1.15 2010-08-11 14:10:52 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


namespace Hiperscan.QuickStep.IO
{

    public class AutoSaveConfig
    {
        private DirectoryProperty single_dir;
        private DirectoryProperty stream_dir;

        public AutoSaveConfig(DirectoryProperty single_dir, DirectoryProperty stream_dir)
        {
            this.single_dir = single_dir;
            this.stream_dir = stream_dir;
            
            this.single_dir.AutoSaveStream = false;
            this.stream_dir.AutoSaveSingle = false;
        }
        
        public void DisconnectSingleDevice()
        {
            if (this.single_dir != null)
                this.single_dir.DisconnectSingleDevice();
        }
        
        public void DisconnectStreamDevice()
        {
            if (this.stream_dir != null)
                this.stream_dir.DisconnectStreamDevice();
        }
        
        public DirectoryProperty SingleDir
        {
            get
            { 
                return this.single_dir; 
            }
            set
            {
                if (this.single_dir != null)
                    this.single_dir.DisconnectSingleDevice();
                this.single_dir = value;
                this.single_dir.AutoSaveStream = false;
            }
        }

        public DirectoryProperty StreamDir
        {
            get
            { 
                return this.stream_dir;
            }
            set
            {
                if (this.stream_dir != null)
                    this.stream_dir.DisconnectStreamDevice();
                this.stream_dir = value;
                this.stream_dir.AutoSaveSingle = false;
            }
        }
    }
}