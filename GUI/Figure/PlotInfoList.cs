// UI.Figure.cs created with MonoDevelop
// User: klose at 14:52 29.01.2009
// CVS release: $Id: UI.Figure.cs,v 1.44 2011-06-15 15:52:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System.Collections.Generic;

using Hiperscan.Extensions;
using Hiperscan.NPlotEngine;


namespace Hiperscan.QuickStep.Figure
{

    public class PlotInfoList : System.Collections.Generic.Dictionary<string, PlotInfo>
    {
        private PlotInfo background = null;
        private readonly List<PlotInfo> list;

        public delegate void CurrentSpectraChangedHandler(CurrentSpectra current_spectra, bool ds_changed);
        public event CurrentSpectraChangedHandler CurrentSpectraChanged;

        public delegate void LastEntryReachedHandler(CurrentSpectra current_spectra);
        public event LastEntryReachedHandler LastEntryReached;

        public delegate void FirstEntryReachedHandler(CurrentSpectra current_spectra);
        public event FirstEntryReachedHandler FirstEntryReached;

        public delegate void UndoPossibleHandler(CurrentSpectra current_spectra);
        public event UndoPossibleHandler UndoPossible;

        public delegate void RedoPossibleHandler(CurrentSpectra current_spectra);
        public event RedoPossibleHandler RedoPossible;


        public PlotInfoList() : base()
        {
            this.BackgroundType = BackgroundType.Unchanged;
            this.list = new List<PlotInfo>();
        }

        public new void Add(string key, PlotInfo item)
        {
            this.ConnectEvents(item.CurrentSpectra);

            base.Add(key, item);
            this.list.Add(item);

            if (this.Current == null)
                this.Current = item;
        }

        public new bool Remove(string key)
        {
            if (this.ContainsKey(key) == false)
                return false;

            this.DisconnectEvents(this[key].CurrentSpectra);
            this.list.Remove(this[key]);
            return base.Remove(key);
        }

        public new void Clear()
        {
            foreach (PlotInfo item in this.Values)
            {
                this.DisconnectEvents(item.CurrentSpectra);
            }
            base.Clear();
            this.list.Clear();
        }

        public void UpdateBackground()
        {
            foreach (PlotInfo item in this.Values)
            {
                if (this.Background != null && item.UUID == this.Background.UUID)
                {
                    item.IsBackground = true;
                }
                else
                {
                    item.ScrolledPlot.Plot.ClearBackgroundPlot();
                    item.IsBackground = false;
                }

                if (this.Background == null || item.UUID == this.Background.UUID)
                {
                    item.ScrolledPlot.Plot.RequestUpdate();
                    continue;
                }

                item.ScrolledPlot.Plot.SetBackgroundPlot(this.Background.ScrolledPlot.Plot, this.BackgroundType);
                item.ScrolledPlot.Plot.RequestUpdate();
            }
        }

        private void ConnectEvents(CurrentSpectra item)
        {
            item.Changed           += this.OnCurrentSpectraChanged;
            item.LastEntryReached  += this.OnLastEntryReached;
            item.FirstEntryReached += this.OnFirstEntryReached;
            item.UndoPossible      += this.OnUndoPossible;
            item.RedoPossible      += this.OnRedoPossible;
        }

        private void DisconnectEvents(CurrentSpectra item)
        {
            item.Changed           -= this.OnCurrentSpectraChanged;
            item.LastEntryReached  -= this.OnLastEntryReached;
            item.FirstEntryReached -= this.OnFirstEntryReached;
            item.UndoPossible      -= this.OnUndoPossible;
            item.RedoPossible      -= this.OnRedoPossible;
        }

        private void OnCurrentSpectraChanged(ICurrentSpectra current_spectra, bool ds_changed)
        {
            this.CurrentSpectraChanged?.Invoke((CurrentSpectra)current_spectra, ds_changed);
        }

        private void OnLastEntryReached(CurrentSpectra current_spectra)
        {
            this.LastEntryReached?.Invoke(current_spectra);
        }

        private void OnFirstEntryReached(CurrentSpectra current_spectra)
        {
            this.FirstEntryReached?.Invoke(current_spectra);
        }

        private void OnUndoPossible(CurrentSpectra current_spectra)
        {
            this.UndoPossible?.Invoke(current_spectra);
        }

        private void OnRedoPossible(CurrentSpectra current_spectra)
        {
            this.RedoPossible?.Invoke(current_spectra);
        }

        public PlotInfo Background
        {
            get
            {
                return this.background;
            }
            set
            {
                this.background = value;
                this.UpdateBackground();
            }
        }

        public PlotInfo this[int ix] => this.list[ix];

        public BackgroundType BackgroundType { get; set; }
        public PlotInfo Current              { get; set; } = null;
    }
}