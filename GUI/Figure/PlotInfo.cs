// UI.Figure.cs created with MonoDevelop
// User: klose at 14:52 29.01.2009
// CVS release: $Id: UI.Figure.cs,v 1.44 2011-06-15 15:52:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Gtk;

using Hiperscan.Extensions;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Figure
{

    public enum AxisType
    {
        Wavelength,
        Wavecount
    }

    public class PlotInfo
    {
        private string name;

        public CurrentSpectra CurrentSpectra { get; private set; }
        public ScrolledPlot ScrolledPlot     { get; private set; }
        public string UUID                   { get; private set; }
        public Gdk.Pixbuf Thumbnail          { get; private set; }
        public TreeIter TreeIter             { get; set; }
        public PlotTypeInfo PlotType         { get; set; }
        public bool IsBackground             { get; set; }

        private DataSet.XConstraints x_constraints;

        public delegate void ThumbnailChangedHandler(PlotInfo plot_info);
        public event ThumbnailChangedHandler ThumbnailChanged;

        public delegate void NameChangedHandler(string name);
        public event NameChangedHandler NameChanged;

        private static int count = 0;


        public PlotInfo(PlotTypeInfo plot_type)
        {
            this.CurrentSpectra = new CurrentSpectra(this);
            this.Name = string.Format(Catalog.GetString("Figure {0}"), ++count);
            this.ScrolledPlot = new ScrolledPlot(this)
            {
                HasPopupDialog = true
            };
            this.UUID = Guid.NewGuid().ToString();
            this.TreeIter = TreeIter.Zero;
            this.PlotType = plot_type;
            this.Thumbnail = this.ScrolledPlot.Plot.Thumbnail;

            this.ScrolledPlot.Plot.DataSetsChanged += this.OnDataSetsChanged;
        }

        ~PlotInfo()
        {
            this.ScrolledPlot.Plot.DataSetsChanged -= this.OnDataSetsChanged;
        }

        private void OnDataSetsChanged(object o, EventArgs e)
        {
            this.Thumbnail = this.ScrolledPlot.Plot.Thumbnail;
            this.ThumbnailChanged?.Invoke(this);
        }

        public string Name
        {
            get 
            { 
                return this.name; 
            }
            set
            {
                if (this.name == value)
                    return;
                this.name = value;

                this.NotifyNameChanged();
            }
        }

        public void NotifyNameChanged()
        {
            if (this.NameChanged == null)
                return;

            if (this.IsBackground)
                this.NameChanged($"{this.Name}*");
            else
                this.NameChanged(this.Name);
        }

        public DataSet.XConstraints XConstraints
        {
            get { return this.x_constraints ?? DataSet.XConstraints.Empty; }
            set { this.x_constraints = value; }
        }
    }
}