﻿// PlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: PlotTypes.cs,v 1.3 2011-01-06 16:36:28 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Hiperscan.Extensions;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Figure
{

    public class UndefinedPlotTypeInfo : PlotTypeInfo
    {
        public UndefinedPlotTypeInfo() : base("Undefined")
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Intensity / a.u.");
            this.LowerLimit = -2.0;
            this.UpperLimit = 5.0;
            this.StandardMin = -0.1;
            this.StandardMax = 2.6;
            this.NeedAbsorbance = false;
        }
    }
    
    public class IntensityPlotTypeInfo : PlotTypeInfo
    {
        public IntensityPlotTypeInfo() : base(Catalog.GetString("Intensity"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Intensity / a.u.");
            this.LowerLimit = -2.0;
            this.UpperLimit = 5.0;
            this.StandardMin = -0.1;
            this.StandardMax = 2.6;
            this.NeedAbsorbance = false;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            DataSet ds = new DataSet(spectrum.Intensity);
            return ds;
        }
    }
    
    public class ReferencePlotTypeInfo : PlotTypeInfo
    {
        public ReferencePlotTypeInfo() : base(Catalog.GetString("Reference"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Reference intensity / a.u.");
            this.LowerLimit = -2.0;
            this.UpperLimit = 5.0;
            this.StandardMin = -0.1;
            this.StandardMax = 2.6;
            this.NeedAbsorbance = false;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            if (!spectrum.HasReference)
                return null;

            return new DataSet(spectrum.Reference.Intensity);
        }
    }

    public class AbsorbancePlotTypeInfo : PlotTypeInfo
    {
        public AbsorbancePlotTypeInfo() : base(Catalog.GetString("Absorbance"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Absorbance");
            this.LowerLimit = -5.0;
            this.UpperLimit = 8.0;
            this.StandardMin = -0.5;
            this.StandardMax = 4.1;
            this.NeedAbsorbance = true;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            if (!spectrum.HasAbsorbance)
                return null;
            
            DataSet ds = new DataSet(spectrum.Absorbance);
            return ds;
        }
    }
    
    public class AbsorbanceSNVPlotTypeInfo : PlotTypeInfo
    {
        public AbsorbanceSNVPlotTypeInfo() : base(Catalog.GetString("SNV(Absorbance)"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("SNV(Absorbance)");
            this.LowerLimit = -5.0;
            this.UpperLimit = 8.0;
            this.StandardMin = -0.5;
            this.StandardMax = 4.1;
            this.NeedAbsorbance = true;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            if (spectrum.HasAbsorbance == false)
                return null;
            
            DataSet range = x_constraints.Apply(spectrum.Absorbance);
            DataSet ds = range.SNV();
            return ds;
        }
    }

    public class ReflectancePlotTypeInfo : PlotTypeInfo
    {
        public ReflectancePlotTypeInfo() : base(Catalog.GetString("Reflectance"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Reflectance");
            this.LowerLimit = -0.1;
            this.UpperLimit = 2.0;
            this.StandardMin = 0.5;
            this.StandardMax = 1.2;
            this.NeedAbsorbance = true;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            if (spectrum.HasAbsorbance == false)
                return null;
            
            return spectrum.Intensity / spectrum.Reference.Intensity;
        }
    }
}