﻿// ScrolledHSPlot.cs created with MonoDevelop
// User: klose at 10:03 27.12.2008
// CVS release: $Id: ScrolledPlot.cs,v 1.27 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

namespace Hiperscan.QuickStep.Figure
{

    public class DragInfo
    {
        private readonly int physical_width;
        private readonly int physical_height;

        private readonly double x_min_start;
        private readonly double x_max_start;
        private readonly double y_min_start;
        private readonly double y_max_start;
        private readonly double x_delta;
        private readonly double y_delta;

        private double new_x_min;
        private double new_x_max;
        private double new_y_min;
        private double new_y_max;

        private readonly ScrolledPlot scrolled_plot;


        public DragInfo(ScrolledPlot scrolled_plot, Gdk.EventButton e)
        {
            this.scrolled_plot = scrolled_plot;

            this.physical_width  = this.scrolled_plot.Plot.PhysicalXMax - this.scrolled_plot.Plot.PhysicalXMin;
            this.physical_height = this.scrolled_plot.Plot.PhysicalYMax - this.scrolled_plot.Plot.PhysicalYMin;

            this.PhysicalXStart = (int)e.X;
            this.PhysicalYStart = (int)e.Y;

            this.x_min_start = scrolled_plot.XMin;
            this.x_max_start = scrolled_plot.XMax;
            this.y_min_start = scrolled_plot.YMin;
            this.y_max_start = scrolled_plot.YMax;

            this.x_delta = this.x_max_start - this.x_min_start;
            this.y_delta = this.y_max_start - this.y_min_start;
        }

        public void ShowPan(Gdk.EventMotion e)
        {
            int physical_delta_x = (int)e.X - this.PhysicalXStart;
            int physical_delta_y = (int)e.Y - this.PhysicalYStart;

            double x_pan = (double)physical_delta_x * this.x_delta / (double)this.physical_width;
            double y_pan = (double)physical_delta_y * this.y_delta / (double)this.physical_height;

            this.new_x_min = this.scrolled_plot.Plot.ReverseXAxes ? this.x_min_start + x_pan : this.x_min_start - x_pan;
            this.new_x_max = this.scrolled_plot.Plot.ReverseXAxes ? this.x_max_start + x_pan : this.x_max_start - x_pan;
            this.new_y_min = this.y_min_start - y_pan;
            this.new_y_max = this.y_max_start - y_pan;

            this.scrolled_plot.Plot.XMin = this.new_x_min;
            this.scrolled_plot.Plot.XMax = this.new_x_max;
            this.scrolled_plot.Plot.YMin = this.new_y_min;
            this.scrolled_plot.Plot.YMax = this.new_y_max;
        }

        public void ApplyPan()
        {
            if (this.new_x_min.Equals(0.0) &&
                this.new_x_max.Equals(0.0) &&
                this.new_y_min.Equals(0.0) &&
                this.new_y_max.Equals(0.0))
            {
                return;
            }

            this.scrolled_plot.SetXYMinMax(this.new_x_min, 
                                           this.new_x_max,
                                           this.new_y_min,
                                           this.new_y_max,
                                           true);
        }

        public int PhysicalXStart { get; }
        public int PhysicalYStart { get; }
    }
}