﻿// Message.cs created with MonoDevelop
// User: klose at 21:14 08.01.2009
// CVS release: $Id: Messages.cs,v 1.19 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Gtk;

using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Dialog
{

    public class InfoMessageDialog : Hiperscan.QuickStep.Dialog.MessageDialog
    {

        public delegate void CloseClickedHandler();

        private InfoMessageDialog(Window parent,
                                  string format,
                                  params object[] args)
            : this (parent, false, format, args)
        {
        }

        private InfoMessageDialog(Window parent,
                                  bool use_markup,
                                  string format,
                                  params object[] args)
            : base(parent, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, format, args)
        {
            this.Title     = $"{MainClass.ApplicationName} {Catalog.GetString("Information")}";
            this.UseMarkup = use_markup;
            this.Present();
            this.Run();
            this.Destroy();
        }

        private InfoMessageDialog(Window parent,
                                  string add_button,
                                  EventHandler add_button_clicked, 
                                  string format,
                                  params object[] args)
            : this(parent, add_button, add_button_clicked, false, format, args)
        {
        }

        private InfoMessageDialog(Window parent,
                                  string add_button,
                                  EventHandler add_button_clicked,
                                  bool use_markup,
                                  string format,
                                  params object[] args)
            : base(parent, DialogFlags.Modal, MessageType.Info, ButtonsType.None, format, args)
        {
            this.Title     = $"{MainClass.ApplicationName} {Catalog.GetString("Information")}";
            this.UseMarkup = use_markup;
            Button b = new Button(add_button);
            b.Clicked += (object sender, EventArgs e) =>
            { 
                this.Destroy(); 
                add_button_clicked.Invoke(sender, e);
            };
            b.ShowAll();
            b.CanFocus = false;
            this.ActionArea.PackStart(b);
            this.AddButton(Stock.Ok, ResponseType.Ok);
            this.Run();
            this.Destroy();
        }

        private InfoMessageDialog(Window parent,
                                  ref bool dont_show_again,
                                  string format,
                                  params object[] args) 
            : this(parent, ref dont_show_again, false, format, args)
        {
        }

        private InfoMessageDialog(Window parent,
                                  ref bool dont_show_again,
                                  bool use_markup, 
                                  string format, 
                                  params object[] args)
            : base(parent, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, format, args)
        {
            if (dont_show_again)
            {
                this.Destroy();
                return;
            }
            
            this.Title     = $"{MainClass.ApplicationName} {Catalog.GetString("Information")}";
            this.UseMarkup = use_markup;

            CheckButton cb = new CheckButton(Catalog.GetString("Don't show this dialog again"))
            {
                CanFocus = false
            };
            this.VBox.PackStart(cb);
            cb.Show();
            
            this.Run();
            dont_show_again = cb.Active;
            this.Destroy();
        }
        
        private InfoMessageDialog(Window parent,
                                  EventHandler h,
                                  bool use_markup,
                                  string format,
                                  params object[] args)
            : base(parent, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, format, args)
        {
            this.Title     = $"{MainClass.ApplicationName} {Catalog.GetString("Information")}";
            this.UseMarkup = use_markup;
            this.Destroyed += h;
            this.Run();
            this.Destroy();
        }

        public static void Show(Window parent, string format, params object[] args)
        {
            var info_message = new InfoMessageDialog(parent, format, args);
        }

        public static void Show(Window parent,
                                bool use_markup,
                                string format,
                                params object[] args)
        {
            var info_message = new InfoMessageDialog(parent, use_markup, format, args);
        }

        public static void Show(Window parent, 
                                string add_button, 
                                EventHandler add_button_clicked,
                                string format,
                                params object[] args)
        {
            var info_message = new InfoMessageDialog(parent,
                                                     add_button, 
                                                     add_button_clicked, 
                                                     format, 
                                                     args);
        }

        public static void Show(Window parent, 
                                string add_button, 
                                EventHandler add_button_clicked,
                                bool use_markup,
                                string format, 
                                params object[] args)
        {
            var info_message = new InfoMessageDialog(parent, add_button,
                                                     add_button_clicked,
                                                     use_markup,
                                                     format,
                                                     args);
        }

        public static void Show(Window parent,
                                ref bool dont_show_again,
                                string format,
                                params object[] args)
        {
            var info_message = new InfoMessageDialog(parent, ref dont_show_again, format, args);
        }

        public static void Show(Window parent,
                                ref bool dont_show_again,
                                bool use_markup,
                                string format,
                                params object[] args)
        {
            var info_message = new InfoMessageDialog(parent,
                                                     ref dont_show_again,
                                                     use_markup,
                                                     format,
                                                     args);
        }

        public static void Show(Window parent,
                                EventHandler h, 
                                bool use_markup,
                                string format,
                                params object[] args)
        {
            var info_message = new InfoMessageDialog(parent, h, use_markup, format, args);
        }
    }
}