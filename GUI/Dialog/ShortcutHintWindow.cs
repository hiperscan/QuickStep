﻿// Message.cs created with MonoDevelop
// User: klose at 21:14 08.01.2009
// CVS release: $Id: Messages.cs,v 1.19 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Gtk;

using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Dialog
{

    public class ShortcutHintWindow : Gtk.Window
    {

        public ShortcutHintWindow(Window parent) : base(WindowType.Popup)
        {
            this.Title = Catalog.GetString("Define Shortcuts");
            
            VBox vbox = new VBox();

            Label l1 = new Label(Catalog.GetString("Define shortcuts by pressing a key (combination) while " +
                                                   "hovering with the mouse cursor over an active menu entry."))
            {
                Wrap = true
            };
            vbox.PackStart(l1);

            vbox.PackStart(new Image(MainClass.Assembly, "GUI.Icons.shortcuts_en.png"));

            Label l2 = new Label(Catalog.GetString("The shortcut will only be set if not already in use " +
                                                   "by QuickStep. To remove shortcuts press the delete key."))
            {
                UseMarkup = true,
                Wrap = true
            };
            vbox.PackStart(l2);

            Label l3 = new Label(Catalog.GetString("<b>Press Esc to exit edit mode.</b>"))
            {
                UseMarkup = true
            };
            vbox.PackStart(l3);
            
            vbox.Spacing = 5;
            this.Add(vbox);

            this.ModifyBg(StateType.Normal, new Gdk.Color(250, 250, 250));
            this.BorderWidth = 10;
            parent.GdkWindow.GetGeometry(out int x, out int y, out int w, out int h, out int d);

            this.ShowAll();
            this.Move(x+w/2-this.SizeRequest().Width/2,
                      y+h  -this.SizeRequest().Height-h/20);
        }
    }
}