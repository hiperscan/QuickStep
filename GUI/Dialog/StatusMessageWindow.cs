﻿// Message.cs created with MonoDevelop
// User: klose at 21:14 08.01.2009
// CVS release: $Id: Messages.cs,v 1.19 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System.Collections.Generic;

using Gtk;


namespace Hiperscan.QuickStep.Dialog
{

    public class StatusMessageWindow : Hiperscan.GtkExtensions.Widgets.RoundedWindow
    {
        public static int XOffset = 0;
        public static int YOffset = 0;
        public static volatile bool EnableMessages = false;
        
        private const int distance = 5;
        private static List<StatusMessageWindow> instances = new List<StatusMessageWindow>();
        
        
        public StatusMessageWindow(Window parent, string text) : this(parent, text, false)
        {
        }
        
        public StatusMessageWindow(Window parent, string text, bool warning) : base(10)
        {
            if (!StatusMessageWindow.EnableMessages)
                return;
            
            if (parent == null || parent.GdkWindow == null)
                return;

            HBox hbox = new HBox
            {
                Spacing = 10
            };

            if (warning)
                hbox.PackStart(new Image(Stock.DialogWarning, IconSize.Dialog));
            else
                hbox.PackStart(new Image(Stock.DialogInfo, IconSize.Dialog));

            Label l = new Label(text)
            {
                WidthRequest = 200,
                LineWrap = true
            };
            hbox.PackStart(l);
            
            this.Title = "Tooltip";
            this.Name  = "gtk-tooltips";
            this.BorderWidth = 10;

            this.Add(hbox);

            int x, y, w, h;
            parent.GetPosition(out x, out y);
            parent.GetSize(out w, out h);

            this.ShowAll();
            this.GdkWindow.GetSize(out int ww, out int wh);
            this.HideAll();

            foreach (StatusMessageWindow status_message in instances)
            {
                status_message.GetPosition(out int root_x, out int root_y);
                status_message.Move(x + w - ww - XOffset, root_y - (wh + distance));
            }

            this.Move(x + w - ww - XOffset,  y + h - wh - YOffset);

            this.ShowAll();
            UserInterface.Instance.ProcessEvents();

            instances.Add(this);
            
            this.ButtonPressEvent += (o, args) => 
            {
            };

            GLib.Timeout.Add(6000, () =>
            {
                instances.Remove(this);
                this.Destroy();
                return false;
            });
        }
        
        protected override bool OnButtonPressEvent (Gdk.EventButton evnt)
        {
            instances.Remove(this);
            this.Destroy();
            return base.OnButtonPressEvent (evnt);
        }
    }
}