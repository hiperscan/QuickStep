﻿// AnnotationSettingsDialog.cs created with MonoDevelop
// User: klose at 10:59 19.01.2010
// CVS release: $Id: AnnotationSettingsDialog.cs,v 1.1 2011-04-07 09:33:57 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Glade;

using Gtk;

using Hiperscan.NPlotEngine.Overlay;
using Hiperscan.Spectroscopy;


namespace Hiperscan.QuickStep.Dialog
{

    public class AnnotationSettingsDialog
    {

        [Widget] Gtk.Dialog annotation_settings_dialog = null;

        [Widget] Gtk.FontButton annotation_text_font_button = null;
        
        [Widget] Gtk.ColorButton annotation_text_color_button  = null;
        [Widget] Gtk.ColorButton annotation_line_color_button  = null;
        [Widget] Gtk.ColorButton annotation_arrow_color_button = null;
        
        [Widget] Gtk.SpinButton annotation_line_width_spin  = null;
        [Widget] Gtk.SpinButton annotation_arrow_width_spin = null;
        
        [Widget] Gtk.ComboBox annotation_line_style_combo  = null;
        [Widget] Gtk.ComboBox annotation_arrow_style_combo = null;
        [Widget] Gtk.ComboBox annotation_cap_style_combo   = null;


        public AnnotationSettingsDialog(Window parent)
        {
            Console.WriteLine("Open new AnnotationSettingsDialog...");
            
            Glade.XML gui;
            
            try
            {
                gui = new Glade.XML(null, "UserInterface.glade", "annotation_settings_dialog", MainClass.ApplicationDomain);
            }
            catch
            {
                Console.WriteLine("UserInterface: No embedded resource for GUI found. Trying to read external Glade file."); 
                gui = new Glade.XML("UserInterface.glade", "annotation_settings_dialog", null);
            }
            gui.Autoconnect(this);
            
            this.annotation_settings_dialog.TransientFor = parent;
            this.annotation_settings_dialog.DeleteEvent += this.OnDeleteEvent;

            ListStore store = new ListStore(typeof(Gdk.Pixbuf), typeof(LineStyle));
            CellRendererPixbuf pixbuf = new CellRendererPixbuf();
            this.annotation_line_style_combo.Clear();
            this.annotation_line_style_combo.PackStart(pixbuf, false);
            this.annotation_line_style_combo.SetAttributes(pixbuf, "pixbuf", 0);

            foreach (LineStyle style in Enum.GetValues(typeof(LineStyle)))
            {
                store.AppendValues(MainWindow.UI.ScrolledPlot.Plot.GetLineStylePixbuf(style), style);
            }
            this.annotation_line_style_combo.Model = store;
            
            store = new ListStore(typeof(Gdk.Pixbuf), typeof(LineStyle));
            pixbuf = new CellRendererPixbuf();
            this.annotation_arrow_style_combo.Clear();
            this.annotation_arrow_style_combo.PackStart(pixbuf, false);
            this.annotation_arrow_style_combo.SetAttributes(pixbuf, "pixbuf", 0);

            foreach (LineStyle style in Enum.GetValues(typeof(LineStyle)))
            {
                store.AppendValues(MainWindow.UI.ScrolledPlot.Plot.GetLineStylePixbuf(style), style);
            }
            this.annotation_arrow_style_combo.Model = store;

            store = new ListStore(typeof(Gdk.Pixbuf), typeof(CapStyle));
            pixbuf = new CellRendererPixbuf();
            this.annotation_cap_style_combo.Clear();
            this.annotation_cap_style_combo.PackStart(pixbuf, false);
            this.annotation_cap_style_combo.SetAttributes(pixbuf, "pixbuf", 0);

            foreach (CapStyle style in Enum.GetValues(typeof(CapStyle)))
            {
                store.AppendValues(MainWindow.UI.ScrolledPlot.Plot.GetLineStylePixbuf(style), style);
            }
            this.annotation_cap_style_combo.Model = store;

            if (string.IsNullOrEmpty(UserInterface.Settings.Current.AnnotationTextDefaultFont) == false)
                this.annotation_text_font_button.SetFontName(UserInterface.Settings.Current.AnnotationTextDefaultFont);

            this.annotation_text_color_button.Color = UserInterface.GdkColor(UserInterface.Settings.Current.AnnotationTextDefaultColor);
            
            this.annotation_line_width_spin.Value   = UserInterface.Settings.Current.AnnotationLineWidth;
            this.annotation_line_color_button.Color = UserInterface.GdkColor(UserInterface.Settings.Current.AnnotationLineColor);
            this.annotation_line_style_combo.Active = (int)UserInterface.Settings.Current.AnnotationLineStyle;
            
            this.annotation_arrow_width_spin.Value   = UserInterface.Settings.Current.AnnotationArrowLineWidth;
            this.annotation_arrow_color_button.Color = UserInterface.GdkColor(UserInterface.Settings.Current.AnnotationArrowColor);
            this.annotation_arrow_style_combo.Active = (int)UserInterface.Settings.Current.AnnotationArrowLineStyle;
            this.annotation_cap_style_combo.Active   = (int)UserInterface.Settings.Current.AnnotationArrowCapStyle;
            
            this.annotation_settings_dialog.ShowAll();
        }
        
        private void Close()
        {
            UserInterface.Settings.Current.AnnotationTextDefaultFont  = this.annotation_text_font_button.FontName;
            UserInterface.Settings.Current.AnnotationTextDefaultColor = UserInterface.DrawingColor(this.annotation_text_color_button.Color);
            
            UserInterface.Settings.Current.AnnotationLineWidth = (float)this.annotation_line_width_spin.Value;
            UserInterface.Settings.Current.AnnotationLineColor = UserInterface.DrawingColor(this.annotation_line_color_button.Color);
            UserInterface.Settings.Current.AnnotationLineStyle = (LineStyle)this.annotation_line_style_combo.Active;

            UserInterface.Settings.Current.AnnotationArrowLineWidth = (float)this.annotation_arrow_width_spin.Value;
            UserInterface.Settings.Current.AnnotationArrowColor     = UserInterface.DrawingColor(this.annotation_arrow_color_button.Color);
            UserInterface.Settings.Current.AnnotationArrowLineStyle = (LineStyle)this.annotation_arrow_style_combo.Active;
            UserInterface.Settings.Current.AnnotationArrowCapStyle  = (CapStyle)this.annotation_cap_style_combo.Active;
            
            this.annotation_settings_dialog.Destroy();
        }
        
        private void OnDeleteEvent(object sender, DeleteEventArgs e)
        {
            e.RetVal = true;
            this.Close();
        }

        internal void OnCloseButtonClicked(object o, EventArgs e)
        {
            this.Close();
        }
    }
}