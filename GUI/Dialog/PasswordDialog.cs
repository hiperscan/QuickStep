﻿// Dialogs.cs created with MonoDevelop
// User: klose at 18:10 12.03.2009
// CVS release: $Id: Dialogs.cs,v 1.25 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Security.Cryptography;
using System.Text;

using Gtk;

using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Dialog
{

    internal class PasswordDialog
    {
        private readonly string title;
        private readonly Window parent;
        
        internal PasswordDialog(Window parent, string title)
        {
            this.parent = parent;
            this.title  = title;
        }
        
        internal string Query()
        {
            var dialog = new Gtk.Dialog(this.title, this.parent, DialogFlags.Modal)
            {
                TransientFor = this.parent,
                Resizable = false
            };
            dialog.AddButton(Stock.Ok, ResponseType.Ok);
            dialog.Default = dialog.ActionArea.Children[0];

            dialog.IconList = MainClass.QuickStepIcons;
            
            Alignment align;
            align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                new Label(Catalog.GetString("Password:"))
            };
            align.TopPadding    = 10;
            align.BottomPadding = 10;
            align.LeftPadding   = 8;
            align.RightPadding  = 8;

            Entry entry = new Entry
            {
                Visibility = false,
                ActivatesDefault = true
            };

            HBox hbox = new HBox();
            hbox.PackStart(align);
            hbox.PackStart(entry);

            dialog.VBox.PackStart(hbox);
            dialog.VBox.ShowAll();
            
            dialog.Run();
            string password = entry.Text;
            dialog.Destroy();
            
            return password;
        }
        
        internal bool Query(string hash)
        {
            string password = this.Query();
            
            // compute md5 hash
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] pass_buf = Encoding.Default.GetBytes(password);
            byte[] hash_buf = md5.ComputeHash(pass_buf);

            return (BitConverter.ToString(hash_buf) == hash);
        }
    }
}