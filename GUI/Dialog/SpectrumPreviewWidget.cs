﻿// Dialogs.cs created with MonoDevelop
// User: klose at 18:10 12.03.2009
// CVS release: $Id: Dialogs.cs,v 1.25 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Gtk;

using Hiperscan.NPlotEngine;
using Hiperscan.QuickStep.Figure;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Dialog
{

    public class SpectrumPreviewWidget : Gtk.Table
    {
        private readonly IPlotEngine plot;

        private readonly ConstCheckButton absorbance_checkbutton;

        private readonly Label device_label;
        private readonly Label di_label;
        private readonly Label date_label;
        private readonly Label comment_label;


        public SpectrumPreviewWidget() : base(5, 2, false)
        {
            // plot
            this.plot = new NPlotEngine.NPlotEngine(PlotEngineType.Preview)
            {
                ReverseXAxes = UserInterface.Settings.Current.AxisType == AxisType.Wavecount
            };

            this.plot.GtkWidget.HeightRequest = 100;
            this.Attach(this.plot.GtkWidget, 0, 2, 0, 1, AttachOptions.Expand, AttachOptions.Expand, 0, 0);

            // absorbance
            Alignment align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                new Label(Catalog.GetString("Absorbance:"))
            };
            this.Attach(align, 0, 1, 1, 2, AttachOptions.Fill, AttachOptions.Shrink, 0, 0);

            this.absorbance_checkbutton = new ConstCheckButton();
            align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                this.absorbance_checkbutton
            };
            this.Attach(align, 1, 2, 1, 2, AttachOptions.Fill, AttachOptions.Shrink, 3, 0);

            // device
            align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                new Label(Catalog.GetString("Device:"))
            };
            this.Attach(align, 0, 1, 2, 3, AttachOptions.Fill, AttachOptions.Shrink, 0, 0);

            this.device_label = new Label();
            align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                this.device_label
            };
            this.Attach(align, 1, 2, 2, 3, AttachOptions.Fill, AttachOptions.Shrink, 3, 0);

            // dark intensity
            align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                new Label(Catalog.GetString("Dark intensity:"))
            };

            this.di_label = new Label();
            align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                this.di_label
            };

            // date
            align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                new Label(Catalog.GetString("Date:"))
            };
            this.Attach(align, 0, 1, 4, 5, AttachOptions.Fill, AttachOptions.Shrink, 0, 0);

            this.date_label = new Label();
            align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                this.date_label
            };
            this.Attach(align, 1, 2, 4, 5, AttachOptions.Fill, AttachOptions.Shrink, 3, 0);

            // comment
            this.comment_label = new Label
            {
                LineWrap = true,
                WidthRequest = 200
            };
            align = new Alignment(0f, 0.5f, 0f, 1f)
            {
                this.comment_label
            };
            this.Attach(align, 0, 2, 5, 6, AttachOptions.Shrink, AttachOptions.Shrink, 0, 5);

            this.ShowAll();
        }

        public Spectrum Spectrum
        {
            set
            {
                this.plot.DataSets.Clear();
                DataSet ds;
                string id;
                if (value.HasAbsorbance)
                {
                    ds = value.Absorbance;
                    id = value.Absorbance.Id;
                }
                else
                {
                    ds = value.Intensity;
                    id = value.Intensity.Id;
                }
                if (UserInterface.Settings.Current.AxisType == AxisType.Wavecount)
                    UserInterface.ConvertXAxisToWavecount(ref ds);

                this.plot.DataSets.Add(id, ds);
                this.plot.FitView();
                this.plot.RequestUpdate();
                this.plot.ProcessChanges();

                this.device_label.Text = value.Serial;

                this.absorbance_checkbutton.Hold = false;
                this.absorbance_checkbutton.Active = value.HasAbsorbance;
                this.absorbance_checkbutton.Inconsistent = false;
                this.absorbance_checkbutton.Hold = true;

                this.di_label.Text = value.HasDarkIntensity.ToString();
                this.date_label.Text = value.Timestamp.ToString();

                if (value.Comment.Length < 100)
                    this.comment_label.Text = value.Comment.Replace('\n', ' ');
                else
                    this.comment_label.Text = value.Comment.Replace('\n', ' ').Substring(0, 97) + "...";
            }
        }

        public Spectrum[] Spectra
        {
            set
            {
                bool absorbance = true;
                bool inconsistent = false;
                bool dark_intensity = false;
                string serial = string.Empty;

                foreach (Spectrum spectrum in value)
                {
                    if (spectrum.HasAbsorbance == false)
                        absorbance = false;

                    if (spectrum.HasAbsorbance && absorbance == false)
                        inconsistent = true;

                    if (spectrum.HasDarkIntensity)
                        dark_intensity = true;

                    if (string.IsNullOrEmpty(serial))
                    {
                        serial = spectrum.Serial;
                    }
                    else
                    {
                        if (serial != spectrum.Serial)
                            serial = Catalog.GetString("n/a");
                    }
                }

                this.plot.DataSets.Clear();

                foreach (Spectrum spectrum in value)
                {
                    if (absorbance && spectrum.HasAbsorbance)
                        this.plot.DataSets.Add(spectrum.Absorbance.Id, spectrum.Absorbance);
                    else if (!absorbance)
                        this.plot.DataSets.Add(spectrum.Intensity.Id, spectrum.Intensity);
                }

                this.plot.FitView();
                this.plot.RequestUpdate();
                this.plot.ProcessChanges();

                this.device_label.Text = serial;

                this.absorbance_checkbutton.Hold = false;
                this.absorbance_checkbutton.Active = absorbance;
                this.absorbance_checkbutton.Inconsistent = inconsistent;
                this.absorbance_checkbutton.Hold = true;

                this.di_label.Text = dark_intensity.ToString();
                this.date_label.Text = Catalog.GetString("n/a");

                this.comment_label.Text = string.Empty;
            }
        }
    }
}