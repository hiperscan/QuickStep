﻿// Dialogs.cs created with MonoDevelop
// User: klose at 18:10 12.03.2009
// CVS release: $Id: Dialogs.cs,v 1.25 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Gtk;

using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Dialog
{

    internal class OverwriteSpectrumDialog : Gtk.MessageDialog
    {
        internal OverwriteSpectrumDialog(Window parent, string path)
            : base(parent,
                   DialogFlags.Modal,
                   MessageType.Question,
                   ButtonsType.YesNo, 
                   string.Format(Catalog.GetString("A file named \"{0}\" already exists. " +
                                                   "Do you want to replace it?"),
                                 path))
        {
            this.TransientFor = parent;
            this.IconList = MainClass.QuickStepIcons;
        }
    }
}