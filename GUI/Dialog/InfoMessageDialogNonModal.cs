﻿// Message.cs created with MonoDevelop
// User: klose at 21:14 08.01.2009
// CVS release: $Id: Messages.cs,v 1.19 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;

using Glade;

using Gtk;

using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Dialog
{

    public class InfoMessageDialogNonModal
    {

        private static InfoMessageDialogNonModal global_instance = null;
        
        private List<string> messages       = new List<string>();
        private List<EventHandler> handlers = new List<EventHandler>();
        private List<DateTime> date_times   = new List<DateTime>();

        private int current_message = 0;
            
        public delegate void CloseClickedHandler();

        [Widget] internal Gtk.Dialog info_message_dialog = null;
        [Widget] internal Gtk.Button prev_button = null;
        [Widget] internal Gtk.Button next_button = null;
        [Widget] internal Gtk.Button info_ok_button = null;
        [Widget] internal Gtk.Button info_close_button = null;
        
        [Widget] internal Gtk.Label message_label = null;
        [Widget] internal Gtk.Label message_info_label = null;

        
        public InfoMessageDialogNonModal(Window parent, 
                                         EventHandler h, 
                                         string format,
                                         params object[] args)
        {
            this.InitializeDialog(parent);
            
            global_instance.messages.Add(string.Format(format, args));
            global_instance.handlers.Add(h);
            global_instance.date_times.Add(DateTime.Now);
            global_instance.current_message = global_instance.messages.Count-1;
            global_instance.Update();
            global_instance.info_message_dialog.ShowAll();
            global_instance.info_message_dialog.Present();
        }
        
        public InfoMessageDialogNonModal(Window parent, string format, params object[] args)
            : this(parent, null, format, args)
        {
        }

        private void InitializeDialog(Window parent)
        {
            if (InfoMessageDialogNonModal.global_instance != null)
                return;
            
            if (parent == null)
                parent = MainClass.MainWindow;
            
            InfoMessageDialogNonModal.global_instance = this;
            
            Glade.XML gui;

            try
            {
                gui = new Glade.XML(null, "UserInterface.glade", "info_message_dialog", MainClass.ApplicationDomain);
            }
            catch
            {
                Console.WriteLine("UserInterface: No embedded resource for GUI found. Trying to read external Glade file."); 
                gui = new Glade.XML("UserInterface.glade", "info_message_dialog", null);
            }
            gui.Autoconnect(this);
            
            if (MainClass.TouchScreen)
            {
                this.info_ok_button.HeightRequest    = MainClass.TOUCH_SCREEN_BUTOON_SIZE;
                this.info_close_button.HeightRequest = MainClass.TOUCH_SCREEN_BUTOON_SIZE;
                this.prev_button.HeightRequest       = MainClass.TOUCH_SCREEN_BUTOON_SIZE;
                this.prev_button.WidthRequest        = MainClass.TOUCH_SCREEN_BUTOON_SIZE;
                this.next_button.HeightRequest       = MainClass.TOUCH_SCREEN_BUTOON_SIZE;
                this.next_button.WidthRequest        = MainClass.TOUCH_SCREEN_BUTOON_SIZE;
            }
            
            this.info_message_dialog.TransientFor = parent;
            this.info_message_dialog.SetPosition(WindowPosition.Center);
            
            this.info_message_dialog.Title = MainClass.ApplicationName + " " + Catalog.GetString("Information");
            this.info_message_dialog.IconList = MainClass.QuickStepIcons;
            
            this.info_message_dialog.Destroyed += (sender, e) =>
            {
                foreach (EventHandler handler in this.handlers)
                {
                    handler?.Invoke(this, EventArgs.Empty);
                }
                InfoMessageDialogNonModal.global_instance = null;
            };
        }
        
        private void Update()
        {
            this.prev_button.Sensitive = (this.current_message > 0);
            this.next_button.Sensitive = (this.current_message < this.messages.Count-1);
            
            this.message_info_label.Text = string.Format(Catalog.GetString("Message {0} of {1}"),
                                                         this.current_message+1,
                                                         this.messages.Count);
            this.message_label.Text =
                $"[{this.date_times[this.current_message].ToString()}]\n{this.messages[this.current_message]}";
        }
        
        internal void OnOkButtonClicked(object o, EventArgs e)
        {
            if (this.messages.Count == 1)
            {
                this.info_message_dialog.Destroy();
                return;
            }

            this.handlers[this.current_message]?.Invoke(this, EventArgs.Empty);

            this.messages.RemoveAt(this.current_message);
            this.handlers.RemoveAt(this.current_message);
            
            if (this.current_message >= this.messages.Count)
                this.current_message = this.messages.Count-1;
            
            this.Update();
        }
        
        internal void OnCloseButtonClicked(object o, System.EventArgs e)
        {
            this.info_message_dialog.Destroy();
        }
        
        internal void OnLastButtonClicked(object o, EventArgs e)
        {
            --this.current_message;
            this.Update();
        }
        
        internal void OnNextButtonClicked(object o, EventArgs e)
        {
            ++this.current_message;
            this.Update();
        }

        public static bool MessageIsShown => InfoMessageDialogNonModal.global_instance != null;
    }    
}
