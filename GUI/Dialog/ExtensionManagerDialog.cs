﻿// ExtensionManager.cs created with MonoDevelop
// User: klose at 14:32 01.07.2010
// CVS release: $Id: ExtensionManager.cs,v 1.3 2011-01-06 16:36:27 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2010  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System.Collections.Generic;

using Gtk;

using Hiperscan.Extensions;
using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Dialog
{

    public class ExtensionManagerDialog : Gtk.Dialog
    {

        public ExtensionManagerDialog()
            : base(Catalog.GetString("Extension Manager"), MainClass.MainWindow, DialogFlags.Modal)
        {
            this.AddButton(Stock.Close, ResponseType.Close);

            this.HasSeparator = true;
            this.WidthRequest = 700;
            this.HeightRequest = 350;

            TreeView tv = new TreeView();
            ListStore store = new ListStore(typeof(bool),
                                            typeof(string),
                                            typeof(string),
                                            typeof(string),
                                            typeof(string),
                                            typeof(string),
                                            typeof(IPlugin));

            store.SetSortColumnId(1, SortType.Ascending);

            CellRendererToggle toggle = new CellRendererToggle();
            toggle.Toggled += (sender, args) =>
            {
                if (store.GetIterFromString(out TreeIter iter, args.Path))
                {
                    bool val = (bool)store.GetValue(iter, 0);
                    IPlugin plugin = (IPlugin)store.GetValue(iter, 6);

                    store.SetValue(iter, 0, !val);
                    plugin.Active = !val;

                    var deactivated_guids = UserInterface.Settings.Current.DeactivatedPluginGuids;
                    var activated_guids   = UserInterface.Settings.Current.ActivatedPluginGuids;

                    if (plugin.Active == false)
                    {
                        if (deactivated_guids.Contains(plugin.Guid.ToString()) == false)
                            deactivated_guids.Add(plugin.Guid.ToString());

                        if (activated_guids.Contains(plugin.Guid.ToString()))
                            activated_guids.Remove(plugin.Guid.ToString());
                    }
                    else
                    {
                        if (deactivated_guids.Contains(plugin.Guid.ToString()))
                            deactivated_guids.Remove(plugin.Guid.ToString());

                        if (activated_guids.Contains(plugin.Guid.ToString()) == false)
                            activated_guids.Add(plugin.Guid.ToString());
                    }

                    UserInterface.Settings.Current.DeactivatedPluginGuids = deactivated_guids;
                    UserInterface.Settings.Current.ActivatedPluginGuids   = activated_guids;

                    MainWindow.UI.UpdatePluginInfos();
                }
            };

            TreeViewColumn column = new TreeViewColumn(string.Empty, toggle, "active", 0)
            {
                Widget = new Image(Stock.Execute, IconSize.Menu)
            };
            column.Widget.Show();

            tv.RulesHint = true;
            tv.AppendColumn(column);
            tv.AppendColumn(Catalog.GetString("Name"),        new CellRendererText(), "text", 1);
            tv.AppendColumn(Catalog.GetString("Version"),     new CellRendererText(), "text", 2);
            tv.AppendColumn(Catalog.GetString("Vendor"),      new CellRendererText(), "text", 3);
            tv.AppendColumn(Catalog.GetString("Description"), new CellRendererText(), "text", 4);
            tv.AppendColumn(Catalog.GetString("UUID"),        new CellRendererText(), "text", 5);


            foreach (IPlugin plugin in MainWindow.UI.plugins.Values)
            {
                store.AppendValues(plugin.Active,
                                   plugin.Name,
                                   plugin.Version,
                                   plugin.Vendor,
                                   plugin.Description,
                                   plugin.Guid.ToString(),
                                   plugin);
            }

            tv.Model = store;

            ScrolledWindow sw = new ScrolledWindow
            {
                HscrollbarPolicy = PolicyType.Automatic,
                VscrollbarPolicy = PolicyType.Automatic
            };
            sw.Add(tv);

            this.VBox.PackStart(sw);
            this.VBox.Homogeneous = false;
            this.ShowAll();

            this.Run();
            this.Destroy();
        }
    }
}