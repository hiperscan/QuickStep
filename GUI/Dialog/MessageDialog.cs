﻿// Message.cs created with MonoDevelop
// User: klose at 21:14 08.01.2009
// CVS release: $Id: Messages.cs,v 1.19 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Gtk;


namespace Hiperscan.QuickStep.Dialog
{

    public class MessageDialog : Gtk.MessageDialog
    {
        public MessageDialog(Window parent,
                             DialogFlags flags,
                             MessageType type,
                             ButtonsType bt,
                             string format,
                             params object[] args)
            : base(parent, flags, type, bt, format, args)
        {
            if (parent != null && parent.IconList != null && parent.IconList.Length != 0)
                this.IconList = parent.IconList;
            else
                this.IconList = MainClass.QuickStepIcons;
            
            this.KeepAbove = true;
        }
        
        public new int Run()
        {
            if (MainClass.TouchScreen)
            {
                foreach (Widget w in this.ActionArea.Children)
                {
                    if (w is Button)
                        w.HeightRequest = MainClass.TOUCH_SCREEN_BUTOON_SIZE;
                }
            }
            
            return base.Run();
        }
    }
}