﻿// Dialogs.cs created with MonoDevelop
// User: klose at 18:10 12.03.2009
// CVS release: $Id: Dialogs.cs,v 1.25 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Gtk;

using Hiperscan.QuickStep.Figure;
using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Dialog
{

    internal class SNVBoundariesDialog
    {
        private const double TYPICAL_RANGE = 3000.0;

        private readonly double globalmin;
        private readonly double globalmax;


        internal SNVBoundariesDialog(double globalmin, double globalmax)
        {
            this.globalmin = globalmin;
            this.globalmax = globalmax;

            if (double.IsInfinity(this.globalmin))
                this.globalmin = 0.0;
            
            if (double.IsInfinity(this.globalmax))
                this.globalmax = this.globalmin + TYPICAL_RANGE;
            
            Console.WriteLine($"{this.globalmin}, {this.globalmax}");
        }
        
        internal UserInterface.SnvInfo Query()
        {
            var dialog = new Gtk.Dialog(Catalog.GetString("Specify SNV boundaries"),
                                        MainClass.MainWindow,
                                        DialogFlags.Modal)
            {
                TransientFor = MainClass.MainWindow
            };
            dialog.AddButton(Stock.Ok, ResponseType.Ok);
            dialog.IconList = MainClass.QuickStepIcons;

            Alignment align;
            Adjustment adjust;
            double init_val;
            Table table;
            SpinButton spin1, spin2;
            
            if (UserInterface.Settings.Current.AxisType == AxisType.Wavecount)
            {
                align = new Alignment(0f, 0.5f, 0f, 1f)
                {
                    new Label(Catalog.GetString("Lower boundary for SNV transformation:"))
                };
                align.LeftPadding  = 8;
                align.RightPadding = 8;
                
                if (UserInterface.WavelengthToWavecount(UserInterface.Settings.Current.SNVUpperBoundary) > this.globalmin)
                    init_val = UserInterface.WavelengthToWavecount(UserInterface.Settings.Current.SNVUpperBoundary);
                else
                    init_val = this.globalmin;
                
                adjust = new Adjustment(init_val, this.globalmin, this.globalmax-1f, 1f, 10f, 0f);
                spin1  = new SpinButton(adjust, 0f, 0);
                
                table = new Table(2, 2, false);
                table.Attach(align, 0, 1, 0, 1);
                table.Attach(spin1, 1, 2, 0, 1);

                align = new Alignment(0f, 0.5f, 0f, 1f)
                {
                    new Label(Catalog.GetString("Upper boundary for SNV transformation:"))
                };
                align.LeftPadding  = 8;
                align.RightPadding = 8;
                
                if (UserInterface.WavelengthToWavecount(UserInterface.Settings.Current.SNVLowerBoundary) < this.globalmax)
                    init_val = UserInterface.WavelengthToWavecount(UserInterface.Settings.Current.SNVLowerBoundary);
                else
                    init_val = this.globalmax;

                adjust = new Adjustment(init_val, this.globalmin+1f, this.globalmax, 1f, 10f, 0f);
                spin2  = new SpinButton(adjust, 0f, 0);
            }
            else
            {
                align = new Alignment(0f, 0.5f, 0f, 1f)
                {
                    new Label(Catalog.GetString("Lower boundary for SNV transformation:"))
                };
                align.LeftPadding  = 8;
                align.RightPadding = 8;
                
                if (UserInterface.Settings.Current.SNVLowerBoundary > this.globalmin)
                    init_val = UserInterface.Settings.Current.SNVLowerBoundary;
                else
                    init_val = this.globalmin;
                
                adjust = new Adjustment(init_val, this.globalmin, this.globalmax-1f, 1f, 10f, 0f);
                spin1  = new SpinButton(adjust, 0f, 0);
                
                table = new Table(2, 2, false);
                table.Attach(align, 0, 1, 0, 1);
                table.Attach(spin1, 1, 2, 0, 1);

                align = new Alignment(0f, 0.5f, 0f, 1f)
                {
                    new Label(Catalog.GetString("Upper boundary for SNV transformation:"))
                };
                align.LeftPadding  = 8;
                align.RightPadding = 8;
                
                if (UserInterface.Settings.Current.SNVUpperBoundary < this.globalmax)
                    init_val = UserInterface.Settings.Current.SNVUpperBoundary;
                else
                    init_val = this.globalmax;

                adjust = new Adjustment(init_val, this.globalmin+1f, this.globalmax, 1f, 10f, 0f);
                spin2  = new SpinButton(adjust, 0f, 0);
            }
            
            table.Attach(align, 0, 1, 1, 2);
            table.Attach(spin2, 1, 2, 1, 2);
            
            dialog.VBox.PackStart(table);
            dialog.VBox.ShowAll();
            
            spin1.ActivatesDefault = true;
            spin2.ActivatesDefault = true;
            dialog.ActionArea.Children[0].CanDefault = true;
            dialog.Default = dialog.ActionArea.Children[0];
            
            ResponseType result = (ResponseType)dialog.Run();
            
            UserInterface.SnvInfo snv_info;
            if (UserInterface.Settings.Current.AxisType == AxisType.Wavecount)
            {
                UserInterface.Settings.Current.SNVLowerBoundary = UserInterface.WavecountToWavelength(spin2.Value);
                UserInterface.Settings.Current.SNVUpperBoundary = UserInterface.WavecountToWavelength(spin1.Value);
                snv_info = new UserInterface.SnvInfo(UserInterface.WavecountToWavelength(spin2.Value),
                                                     UserInterface.WavecountToWavelength(spin1.Value));                
            }
            else
            {
                UserInterface.Settings.Current.SNVLowerBoundary = spin1.Value;
                UserInterface.Settings.Current.SNVUpperBoundary = spin2.Value;
                snv_info = new UserInterface.SnvInfo(spin1.Value, spin2.Value);
            }
            
            dialog.Destroy();
            
            if (result == ResponseType.Ok)
                return snv_info; 
            else
                return new UserInterface.SnvInfo(false);
        }
    }
}