﻿// Message.cs created with MonoDevelop
// User: klose at 21:14 08.01.2009
// CVS release: $Id: Messages.cs,v 1.19 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Gtk;

using Hiperscan.Unix;


namespace Hiperscan.QuickStep.Dialog
{

    public class ErrorMessageDialog : Gtk.MessageDialog
    {

        private readonly TextBuffer text_buffer;
        private bool run_dialog = true;
        
        
        public ErrorMessageDialog(Window parent, Exception ex, string format, params object[] args)
            : base(parent, DialogFlags.Modal, MessageType.Error, ButtonsType.None, format, args)
        {
            this.Title     = $"{MainClass.ApplicationName} {Catalog.GetString("Error")}";
            this.IconList  = MainClass.QuickStepIcons;
            this.Resizable = true;
            
            this.AddButton(Stock.Copy,  0);
            this.AddButton(Stock.Close, 1);
            this.Response += this.OnResponse;

            TextView view = new TextView
            {
                Editable = false
            };
            view.Buffer.Text  = $"Operating system: {MainClass.OS.ToString()}\n";
            view.Buffer.Text += $"Gtk Theme: {Settings.Default.ThemeName}\n";
            view.Buffer.Text += $"Mono Runtime Version: {MainClass.MonoVersionInfo}\n";
            view.Buffer.Text += $"Execution engine: {System.Environment.Version}\n";
            view.Buffer.Text += $"QuickStep version: {MainClass.Assembly.GetName().Version.ToString()}\n";
            view.Buffer.Text += $"Build date: {MainClass.BuildDate}\n";
            view.Buffer.Text += ex.ToString();
            view.Buffer.Text += "\nLoaded plug-ins:\n";

            if (MainWindow.UI != null)
                view.Buffer.Text += MainWindow.UI.GeneratePluginInfo();

            view.Buffer.SelectRange(view.Buffer.StartIter, view.Buffer.EndIter);
            view.HeightRequest = 150;
            
            this.text_buffer = view.Buffer;

            ScrolledWindow scrolled = new ScrolledWindow
            {
                view
            };

            Frame frame = new Frame
            {
                scrolled
            };

            this.VBox.PackStart(frame);
            
            this.ShowAll();
            
            while (this.run_dialog)
            {
                this.Run();
            }

            this.Destroy();
        }
        
        private void OnResponse(object o, ResponseArgs e)
        {
            switch ((int)e.ResponseId)
            {
                
            case 0:
                this.text_buffer.SelectRange(this.text_buffer.StartIter, this.text_buffer.EndIter);
                this.text_buffer.CopyClipboard(Clipboard.Get(Gdk.Selection.Clipboard));
                break;
                
            default:
                this.run_dialog = false;
                break;
                
            }
        }
    }
}