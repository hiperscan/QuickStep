       Release Notes for QuickStep 0.99
            Copyright (c) 2009-2013
       Hiperscan GmbH Dresden, Germany
              All Rights Reserved
===============================================


Thank you for using our products. This file
contains licensing information about QuickStep.

This program is free software: you can
redistribute it and/or modify it under the
terms of the GNU General Public License as
published by the Free Software Foundation,
either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that
it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU
General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

To receive a copy of QuickStep's source code,
please send a request to info@hiperscan.com
or
  HiperScan GmbH
  Weißeritzstraße 3
  01067 Dresden
  GERMANY

Please report bugs, patches and suggestions to
our bug tracking system at 
https://t2969.greatnet.de
