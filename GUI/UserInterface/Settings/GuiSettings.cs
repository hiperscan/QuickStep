﻿// UI.Configuration.cs created with MonoDevelop
// User: klose at 19:06 08.01.2009
// CVS release: $Id: UI.Configuration.cs,v 1.47 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using Hiperscan.NPlotEngine.Overlay;
using Hiperscan.QuickStep.Figure;
using Hiperscan.QuickStep.IO;
using Hiperscan.Spectroscopy;


namespace Hiperscan.QuickStep
{

    public partial class UserInterface
    {

        internal sealed class GuiSettings : System.Configuration.ConfigurationSection
        {

            private static class PathList
            {

                public static string Combine(List<string> paths)
                {
                    StringBuilder result = new StringBuilder();

                    foreach (string path in paths)
                    {
                        result.AppendFormat("{0}{1}", path, Path.PathSeparator);
                    }

                    if (result.Length == 0)
                        return result.ToString();

                    result.Remove(result.Length - 1, 1);

                    if (result.Length == 0)
                        return result.ToString();

                    if (result[0] == Path.PathSeparator)
                        result.Remove(0, 1);

                    return result.ToString();
                }

                public static List<string> Split(string paths)
                {
                    if (paths == null)
                        return new List<string>();

                    Regex regex = new Regex(Path.PathSeparator.ToString());
                    return new List<string>(regex.Split(paths));
                }
            }

            [ConfigurationProperty("SectionExists")]
            public bool SectionExists
            {
                get { return (bool)this["SectionExists"]; }
                set { this["SectionExists"] = value;      }
            }

            [ConfigurationProperty("AutoConnect")]
            public bool AutoConnect
            {
                get { return (bool)this["AutoConnect"]; }
                set { this["AutoConnect"] = value;      }
            }

            [ConfigurationProperty("AutoSelect")]
            public bool AutoSelect
            {
                get { return (bool)this["AutoSelect"]; }
                set { this["AutoSelect"] = value;      }
            }

            [ConfigurationProperty("StandardToolbarVisible")]
            public bool StandardToolbarVisible
            {
                get
                {
                    if ((bool)this["SectionExists"] == false)
                        return true;
                    else
                        return (bool)this["StandardToolbarVisible"];
                }
                set { this["StandardToolbarVisible"] = value; }
            }

            [ConfigurationProperty("FigureToolbarVisible")]
            public bool FigureToolbarVisible
            {
                get
                {
                    if ((bool)this["SectionExists"] == false)
                        return true;
                    else
                        return (bool)this["FigureToolbarVisible"];
                }
                set { this["FigureToolbarVisible"] = value; }
            }

            [ConfigurationProperty("ExtensionsToolbarVisible")]
            public bool ExtensionsToolbarVisible
            {
                get
                {
                    if ((bool)this["SectionExists"] == false)
                        return true;
                    else
                        return (bool)this["ExtensionsToolbarVisible"];
                }
                set { this["ExtensionsToolbarVisible"] = value; }
            }

            [ConfigurationProperty("DataSourcesVisible")]
            public bool DataSourcesVisible
            {
                get
                {
                    if ((bool)this["SectionExists"] == false)
                        return true;
                    else
                        return (bool)this["DataSourcesVisible"];
                }
                set { this["DataSourcesVisible"] = value; }
            }

            [ConfigurationProperty("PropertyEditorVisible")]
            public bool PropertyEditorVisible
            {
                get { return (bool)this["PropertyEditorVisible"]; }
                set { this["PropertyEditorVisible"] = value;      }
            }

            [ConfigurationProperty("PlotBrowserVisible")]
            public bool PlotBrowserVisible
            {
                get { return (bool)this["PlotBrowserVisible"]; }
                set { this["PlotBrowserVisible"] = value;      }
            }

            [ConfigurationProperty("SaveFilePath")]
            public string SaveFilePath
            {
                get { return (string)this["SaveFilePath"]; }
                set { this["SaveFilePath"] = value;        }
            }

            [ConfigurationProperty("ContainerSaveFilePath")]
            public string ContainerSaveFilePath
            {
                get { return (string)this["ContainerSaveFilePath"]; }
                set { this["ContainerSaveFilePath"] = value;        }
            }

            [ConfigurationProperty("ExportFilePath")]
            public string ExportFilePath
            {
                get { return (string)this["ExportFilePath"]; }
                set { this["ExportFilePath"] = value;        }
            }

            [ConfigurationProperty("SaveFrequencyFilePath")]
            public string SaveFrequencyFilePath
            {
                get { return (string)this["SaveFrequencyFilePath"]; }
                set { this["SaveFrequencyFilePath"] = value;        }
            }

            [ConfigurationProperty("SaveAddDataFilePath")]
            public string SaveAddDataFilePath
            {
                get { return (string)this["SaveAddDataFilePath"]; }
                set { this["SaveAddDataFilePath"] = value;        }
            }

            [ConfigurationProperty("OpenFilePath")]
            public string OpenFilePath
            {
                get { return (string)this["OpenFilePath"]; }
                set { this["OpenFilePath"] = value;        }
            }

            [ConfigurationProperty("OpenBgFilePath")]
            public string OpenBgFilePath
            {
                get { return (string)this["OpenBgFilePath"]; }
                set { this["OpenBgFilePath"] = value;        }
            }

            [ConfigurationProperty("OpenDirectoryPath")]
            public string OpenDirectoryPath
            {
                get { return (string)this["OpenDirectoryPath"]; }
                set { this["OpenDirectoryPath"] = value;        }
            }

            [ConfigurationProperty("AutoSaveSingleDirectory")]
            public string AutoSaveSingleDirectory
            {
                get { return (string)this["AutoSaveSingleDirectory"]; }
                set { this["AutoSaveSingleDirectory"] = value;        }
            }

            [ConfigurationProperty("AutoSaveStreamDirectory")]
            public string AutoSaveStreamDirectory
            {
                get { return (string)this["AutoSaveStreamDirectory"]; }
                set { this["AutoSaveStreamDirectory"] = value;        }
            }

            [ConfigurationProperty("WriteToEEPROM")]
            public bool WriteToEEPROM
            {
                get { return (bool)this["WriteToEEPROM"]; }
                set { this["WriteToEEPROM"] = value;      }
            }

            [ConfigurationProperty("DataBrowserPaths")]
            private string __DataBrowserPaths
            {
                get { return (string)this["DataBrowserPaths"]; }
                set { this["DataBrowserPaths"] = value;        }
            }

            public List<string> DataBrowserPaths
            {
                get { return PathList.Split(this.__DataBrowserPaths);    }
                set { this.__DataBrowserPaths = PathList.Combine(value); }
            }

            [ConfigurationProperty("ReopenPaths")]
            private string __ReopenPaths
            {
                get { return (string)this["ReopenPaths"]; }
                set { this["ReopenPaths"] = value;        }
            }

            public List<string> ReopenPaths
            {
                get { return PathList.Split(this.__ReopenPaths);    }
                set { this.__ReopenPaths = PathList.Combine(value); }
            }

            [ConfigurationProperty("DeactivatedPluginGuids")]
            private string __DeactivatedPluginGuids
            {
                get { return (string)this["DeactivatedPluginGuids"]; }
                set { this["DeactivatedPluginGuids"] = value;        }
            }

            public List<string> DeactivatedPluginGuids
            {
                get { return PathList.Split(this.__DeactivatedPluginGuids);    }
                set { this.__DeactivatedPluginGuids = PathList.Combine(value); }
            }

            [ConfigurationProperty("ActivatedPluginGuids")]
            private string __ActivatedPluginGuids
            {
                get { return (string)this["ActivatedPluginGuids"]; }
                set { this["ActivatedPluginGuids"] = value;        }
            }

            public List<string> ActivatedPluginGuids
            {
                get { return PathList.Split(this.__ActivatedPluginGuids);    }
                set { this.__ActivatedPluginGuids = PathList.Combine(value); }
            }

            [ConfigurationProperty("StandardOutputCulture")]
            public OutputCulture StandardOutputCulture
            {
                get
                {
                    if ((bool)this["SectionExists"] == false)
                        return OutputCulture.System;

                    return (OutputCulture)this["StandardOutputCulture"];
                }
                set
                {
                    this["StandardOutputCulture"] = value;
                }
            }

            [ConfigurationProperty("SingleFileMask")]
            public string SingleFileMask
            {
                get
                {
                    if ((string)this["SingleFileMask"] == "")
                        return "single_%time%_%serial%";
                    else
                        return (string)this["SingleFileMask"];
                }
                set { this["SingleFileMask"] = value; }
            }

            [ConfigurationProperty("StreamFileMask")]
            public string StreamFileMask
            {
                get
                {
                    if ((string)this["StreamFileMask"] == "")
                        return "stream_%number%_%time%_%serial%";
                    else
                        return (string)this["StreamFileMask"];
                }
                set { this["StreamFileMask"] = value; }
            }

            [ConfigurationProperty("ShowPreviewInPlot")]
            public bool ShowPreviewInPlot
            {
                get { return (bool)this["ShowPreviewInPlot"]; }
                set { this["ShowPreviewInPlot"] = value;      }
            }

            [ConfigurationProperty("AlwaysShowLegend")]
            public bool AlwaysShowLegend
            {
                get { return (bool)this["AlwaysShowLegend"]; }
                set { this["AlwaysShowLegend"] = value;      }
            }

            [ConfigurationProperty("SNVLowerBoundary")]
            public double SNVLowerBoundary
            {
                get
                {
                    if (((double)this["SNVLowerBoundary"]).Equals(0.0))
                        return 1050f;
                    return (double)this["SNVLowerBoundary"];
                }
                set { this["SNVLowerBoundary"] = value; }
            }

            [ConfigurationProperty("SNVUpperBoundary")]
            public double SNVUpperBoundary
            {
                get
                {
                    if (((double)this["SNVUpperBoundary"]).Equals(0.0))
                        return 1850f;
                    return (double)this["SNVUpperBoundary"];
                }
                set { this["SNVUpperBoundary"] = value; }
            }

            [ConfigurationProperty("MainWindowWidth")]
            public int MainWindowWidth
            {
                get { return (int)this["MainWindowWidth"]; }
                set { this["MainWindowWidth"] = value;     }
            }

            [ConfigurationProperty("MainWindowHeight")]
            public int MainWindowHeight
            {
                get { return (int)this["MainWindowHeight"]; }
                set { this["MainWindowHeight"] = value;     }
            }

            [ConfigurationProperty("MainWindowMaximized")]
            public bool MainWindowMaximized
            {
                get { return (bool)this["MainWindowMaximized"]; }
                set { this["MainWindowMaximized"] = value;      }
            }

            [ConfigurationProperty("SpectrometerPanedPosition")]
            public int SpectrometerPanedPosition
            {
                get
                {
                    if ((bool)this["SectionExists"] == false)
                        return 300;
                    else
                        return (int)this["SpectrometerPanedPosition"];
                }
                set { this["SpectrometerPanedPosition"] = value; }
            }

            [ConfigurationProperty("DataSourcePanedPosition")]
            public int DataSourcePanedPosition
            {
                get { return (int)this["DataSourcePanedPosition"]; }
                set { this["DataSourcePanedPosition"] = value;     }
            }

            [ConfigurationProperty("DontShowEditShortcutMessageAgain")]
            public bool DontShowEditShortcutMessageAgain
            {
                get { return (bool)this["DontShowEditShortcutMessageAgain"]; }
                set { this["DontShowEditShortcutMessageAgain"] = value;      }
            }

            [ConfigurationProperty("RemoteUSBSharePort")]
            public uint RemoteUSBSharePort
            {
                get { return (uint)this["RemoteUSBSharePort"]; }
                set { this["RemoteUSBSharePort"] = value;      }
            }

            [ConfigurationProperty("RemoteUSBServer")]
            public string RemoteUSBServer
            {
                get { return (string)this["RemoteUSBServer"]; }
                set { this["RemoteUSBServer"] = value;        }
            }

            [ConfigurationProperty("RemoteUSBServerPort")]
            public uint RemoteUSBServerPort
            {
                get { return (uint)this["RemoteUSBServerPort"]; }
                set { this["RemoteUSBServerPort"] = value;      }
            }

            [ConfigurationProperty("AnnotationTextDefaultFont")]
            public string AnnotationTextDefaultFont
            {
                get { return (string)this["AnnotationTextDefaultFont"]; }
                set { this["AnnotationTextDefaultFont"] = value;        }
            }

            [ConfigurationProperty("AnnotationTextDefaultColor")]
            public Color AnnotationTextDefaultColor
            {
                get
                {
                    if (this["AnnotationTextDefaultColor"] == null)
                        return Color.Black;
                    return (Color)this["AnnotationTextDefaultColor"];
                }
                set { this["AnnotationTextDefaultColor"] = value; }
            }

            [ConfigurationProperty("AnnotationLineWidth")]
            public float AnnotationLineWidth
            {
                get
                {
                    if ((float)this["AnnotationLineWidth"] <= 0f)
                        return 1f;
                    return (float)this["AnnotationLineWidth"];
                }
                set { this["AnnotationLineWidth"] = value; }
            }

            [ConfigurationProperty("AnnotationLineStyle")]
            public LineStyle AnnotationLineStyle
            {
                get { return (LineStyle)this["AnnotationLineStyle"]; }
                set { this["AnnotationLineStyle"] = value;           }
            }

            [ConfigurationProperty("AnnotationLineColor")]
            public Color AnnotationLineColor
            {
                get
                {
                    if (this["AnnotationLineColor"] == null)
                        return Color.Black;
                    return (Color)this["AnnotationLineColor"];
                }
                set { this["AnnotationLineColor"] = value; }
            }

            [ConfigurationProperty("AnnotationArrowLineWidth")]
            public float AnnotationArrowLineWidth
            {
                get
                {
                    if ((float)this["AnnotationArrowLineWidth"] <= 0f)
                        return 2f;
                    return (float)this["AnnotationArrowLineWidth"];
                }
                set { this["AnnotationArrowLineWidth"] = value; }
            }

            [ConfigurationProperty("AnnotationArrowLineStyle")]
            public LineStyle AnnotationArrowLineStyle
            {
                get { return (LineStyle)this["AnnotationArrowLineStyle"]; }
                set { this["AnnotationArrowLineStyle"] = value;           }
            }

            [ConfigurationProperty("AnnotationArrowColor")]
            public Color AnnotationArrowColor
            {
                get
                {
                    if (this["AnnotationArrowColor"] == null)
                        return Color.Black;
                    return (Color)this["AnnotationArrowColor"];
                }
                set { this["AnnotationArrowColor"] = value; }
            }

            [ConfigurationProperty("AnnotationArrowCapStyle")]
            public CapStyle AnnotationArrowCapStyle
            {
                get { return (CapStyle)this["AnnotationArrowCapStyle"]; }
                set { this["AnnotationArrowCapStyle"] = value;          }
            }

            [Obsolete]
            [ConfigurationProperty("ThemeName")]
            public string ThemeName
            {
#pragma warning disable RECS0029
                get
                {
                    if (Settings.saving == false)
                        throw new Exception("ConfigurationProperty \"ThemeName\" is deprecated. Use \"GtkTheme\".");
                    return string.Empty;
                }
                set
                {
                    if (Settings.saving == false)
                        throw new Exception("ConfigurationProperty \"ThemeName\" is deprecated. Use \"GtkTheme\".");
                }
#pragma warning restore RECS0029
            }

            [ConfigurationProperty("GtkTheme")]
            public string GtkTheme
            {
                get { return (string)this["GtkTheme"]; }
                set { this["GtkTheme"] = value;        }
            }

            [ConfigurationProperty("DontShowDiscardPropertiesDialogAgain")]
            public bool DontShowDiscardPropertiesDialogAgain
            {
                get { return (bool)this["DontShowDiscardPropertiesDialogAgain"]; }
                set { this["DontShowDiscardPropertiesDialogAgain"] = value;      }
            }

            [ConfigurationProperty("AxisType")]
            public AxisType AxisType
            {
                get { return (AxisType)this["AxisType"]; }
                set { this["AxisType"] = value;          }
            }

            [ConfigurationProperty("XConstraints")]
            private string _XConstraints
            {
                get { return (string)this["XConstraints"]; }
                set { this["XConstraints"] = value;        }
            }

            public List<string> XConstraints
            {
                get
                {
                    if (string.IsNullOrEmpty(_XConstraints))
                        return new List<string>();
                    string[] constraints = _XConstraints.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    return new List<string>(constraints);
                }
                set
                {
                    if (value.Count > MAX_X_CONSTRAINTS_COUNT)
                        value.RemoveRange(0, value.Count - MAX_X_CONSTRAINTS_COUNT);
                    _XConstraints = string.Join(":", value.ToArray());
                }
            }
        }
    }
}