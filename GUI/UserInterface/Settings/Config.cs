// UI.Configuration.cs created with MonoDevelop
// User: klose at 19:06 08.01.2009
// CVS release: $Id: UI.Configuration.cs,v 1.47 2011-04-19 12:36:58 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System.Drawing;


namespace Hiperscan.QuickStep
{

    public partial class UserInterface
    {

        public static class Config
        {

            public enum PropertyType
            {
                Spectrometer,
                Directory,
                Spectrum
            }

            public enum ApplicationMode
            {
                Normal,
                Exhibition
            }

            public static readonly Color PreviewColor = Color.Aqua;
            public static readonly Color StreamColor  = Color.Red;
        }
    }
}