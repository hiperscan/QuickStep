// UI.Toolbar.cs created with MonoDevelop
// User: klose at 10:34 15.01.2009
// CVS release: $Id: UI.Toolbar.cs,v 1.75 2011-06-24 13:48:23 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Gtk;

using Hiperscan.Spectroscopy;


namespace Hiperscan.QuickStep
{

    public partial class UserInterface
    {

        public class XConstraintsCompletion : Gtk.EntryCompletion
        {
            public event EventHandler Selected;

            protected override bool OnMatchSelected(TreeModel model, TreeIter iter)
            {
                this.Selected?.Invoke(this, EventArgs.Empty);
                return base.OnMatchSelected(model, iter);
            }
        }

        public class XConstraintsEntry : Gtk.Entry
        {
            public event EventHandler ReturnPressed;

            private Gdk.Color? standard_text_color = null;
            private Gdk.Color? standard_base_color = null;
            private Gdk.Color error_text_color;
            private Gdk.Color error_base_color;


            public XConstraintsEntry(Gdk.Color error_text_color, Gdk.Color error_base_color) : base()
            {
                this.error_text_color = error_text_color;
                this.error_base_color = error_base_color;

                this.Completion = new XConstraintsCompletion
                {
                    Model = new ListStore(typeof(string)),
                    TextColumn = 0
                };

                this.UpdateCompletion();
            }

            public void UpdateCompletion()
            {
                var constraints = UserInterface.Settings.Current.XConstraints;
                constraints.Reverse();
                ListStore store = (ListStore)this.Completion.Model;
                store.Clear();
                foreach (string constraint in constraints)
                {
                    store.AppendValues(constraint);
                }
            }

            protected override void OnChanged()
            {
                if (this.standard_text_color == null)
                    this.standard_text_color = this.Style.Text(StateType.Normal);

                if (this.standard_base_color == null)
                    this.standard_base_color = this.Style.Base(StateType.Normal);

                if (string.IsNullOrEmpty(this.Text) || DataSet.XConstraints.CreateFromString(this.Text) != null)
                {
                    this.ModifyText(StateType.Normal, (Gdk.Color)this.standard_text_color);
                    this.ModifyBase(StateType.Normal, (Gdk.Color)this.standard_base_color);
                }
                else
                {
                    this.ModifyText(StateType.Normal, this.error_text_color);
                    this.ModifyBase(StateType.Normal, this.error_base_color);
                }

                base.OnChanged();
            }

            protected override bool OnKeyPressEvent(Gdk.EventKey evnt)
            {
                if ((evnt.Key == Gdk.Key.Return || evnt.Key == Gdk.Key.KP_Enter) && this.ReturnPressed != null)
                    this.ReturnPressed(null, EventArgs.Empty);

                return base.OnKeyPressEvent(evnt);
            }

            protected override bool OnButtonPressEvent(Gdk.EventButton evnt)
            {
                return base.OnButtonPressEvent(evnt);
            }

            public XConstraintsCompletion _XConstraintsCompletion => this.Completion as XConstraintsCompletion;
        }
    }
}
