// UI.Extensions.cs created with MonoDevelop
// User: klose at 14:32 01.07.2010
// CVS release: $Id: UI.Extensions.cs,v 1.11 2011-04-19 12:36:59 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2010  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Gtk;

using Hiperscan.Extensions;
using Hiperscan.QuickStep.Dialog;
using Hiperscan.Unix;


namespace Hiperscan.QuickStep
{

    public partial class UserInterface
    {

        private class PluginRunner
        {
            private readonly UserInterface ui;
            private readonly IPlugin plugin;

            private ToolbarInfo toolbar_info;
            private MenuInfo    menu_info;


            public PluginRunner(UserInterface ui, IPlugin plugin, ToolbarInfo toolbar_info)
            {
                this.ui = ui;
                this.plugin = plugin;
                this.toolbar_info = toolbar_info;
                this.menu_info = null;
            }

            public PluginRunner(UserInterface ui, IPlugin plugin, MenuInfo menu_info)
            {
                this.ui = ui;
                this.plugin = plugin;
                this.toolbar_info = null;
                this.menu_info = menu_info;
            }

            public void Invoke(object sender, EventArgs e)
            {
                bool cursor_changed  = false;
                Widget locked_widget = null;

                try
                {
                    if (this.toolbar_info != null)
                    {
                        if (this.toolbar_info.WatchCursor)
                        {
                            this.ui.SetWatchCursor(null, null);
                            UserInterface.Instance.ProcessEvents();
                            cursor_changed = true;
                        }
                        toolbar_info.ToolItem.Sensitive = false;
                        locked_widget = toolbar_info.ToolItem;
                        toolbar_info.Invoke(sender, e);
                    }

                    if (this.menu_info != null)
                    {
                        if (this.menu_info.WatchCursor)
                        {
                            this.ui.SetWatchCursor(null, null);
                            UserInterface.Instance.ProcessEvents();
                            cursor_changed = true;
                        }
                        menu_info.MenuItem.Sensitive = false;
                        locked_widget = menu_info.MenuItem;
                        menu_info.Invoke(sender, e);
                    }
                }
                catch (EmergencyStopException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception while executing extension '{0}':\n{1}", this.plugin.Name, ex.ToString());
                    InfoMessageDialog.Show(this.ui.MainWindow,
                                           Catalog.GetString("An error ocurred while invoking QuickStep extension '{0}': {1}"),
                                           this.plugin.Name, ex.Message);
                }
                finally
                {
                    if (cursor_changed)
                        this.ui.SetArrowCursor(null, null);

                    if (locked_widget != null)
                        locked_widget.Sensitive = true;

                    this.ui.UpdateDeviceControls();
                }
            }
        }
    }
}