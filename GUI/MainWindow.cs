// Main.cs created with MonoDevelop
// User: klose at 13:54 16.12.2008
// CVS release: $Id: Main.cs,v 1.68 2011-06-24 13:48:23 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Gtk;


namespace Hiperscan.QuickStep
{

    public class MainWindow : Gtk.Window
    {

        public static UserInterface UI;


        public MainWindow(MainClass.IconType icon_type) : base (Gtk.WindowType.Toplevel)
        {
            this.IconList = MainClass.QuickStepIcons;
            this.Title    = $"{MainClass.ApplicationName} {MainClass.TitleExtension}";

            string wm_suffix = string.Empty;
            if (icon_type != MainClass.IconType.Default)
                wm_suffix = $"_{icon_type.ToString().ToLower()}";

            this.SetWmclass(MainClass.ApplicationName.ToLower() + wm_suffix,
                            MainClass.ApplicationName.ToLower() + wm_suffix);


            MainWindow.UI = new UserInterface(this);
            this.Add(MainWindow.UI.GUI);

            this.DeleteEvent += this.OnDeleteEvent;
        }
        
        private void OnDeleteEvent(object sender, DeleteEventArgs e)
        {
            e.RetVal = true;
            if (string.IsNullOrEmpty(MainClass.AutoPlugin))
                MainClass.Quit(false);
            else 
                MainClass.MainWindow.Hide();
        }
    }
}