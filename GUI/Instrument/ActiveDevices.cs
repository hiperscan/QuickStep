// ActiveDevices.cs created with MonoDevelop
// User: klose at 11:03 13.07.2010
// CVS release: $Id: ActiveDevices.cs,v 1.2 2010-07-13 15:12:32 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System.Collections.Generic;
using System.Linq;

using Hiperscan.Extensions;
using Hiperscan.SGS;
using Hiperscan.SGS.Common;


namespace Hiperscan.QuickStep.Instrument
{

    public class ActiveDevices : System.Collections.Generic.List<IDevice>, Hiperscan.Extensions.IActiveDevices
    {
        public event ActiveDevicesChangedHandler Changed;

        public ActiveDevices() : base()
        {
        }
        
        public ActiveDevices(List<IDevice> devices) : base(devices) 
        {
        }

        public List<IDevice> Idle         => this.Where(d => d.GetState() == Spectrometer.State.Idle).ToList();
        public List<IDevice> Connected    => this.Where(d => d.GetState() != Spectrometer.State.Disconnected).ToList();
        public List<IDevice> Unprogrammed => this.Where(d => d.GetState() == Spectrometer.State.Unprogrammed).ToList();

        public void NotifyChanged() => this.Changed?.Invoke(this);
    }
}