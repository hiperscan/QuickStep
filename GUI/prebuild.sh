#!/bin/sh

set -e

#perl -p -i -e "s/swapped=\"no\"//" UserInterface.glade
cp Properties/AssemblyInfo.template Properties/AssemblyInfo.generated.cs
perl -p -i -e "s/AssemblyInformationalVersion\(\".*\"\)/AssemblyInformationalVersion\(\"`date`\"\)/" Properties/AssemblyInfo.generated.cs
