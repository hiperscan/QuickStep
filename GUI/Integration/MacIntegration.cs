﻿// MacIntegration.cs created with MonoDevelop
// User: klose at 13:13 26.05.2011
// CVS release: $Id: MacIntegration.cs,v 1.1 2011-06-14 12:24:36 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Runtime.InteropServices;

using Gtk;


namespace Hiperscan.QuickStep.Integration
{

    public static class MacIntegration
    {
        
        private class MacMenuGroup : GLib.Opaque
        {
            public MacMenuGroup(IntPtr raw) : base (raw)
            {
            }
            
            [DllImport("libigemacintegration.dylib")]
            private static extern void ige_mac_menu_add_app_menu_item(IntPtr raw, IntPtr menu_item, IntPtr label);


            public void AddMenuItem(MenuItem menu_item, string label)
            {
                IntPtr label_handle = GLib.Marshaller.StringToPtrGStrdup(label);
                try 
                {
                    if (menu_item == null)
                        ige_mac_menu_add_app_menu_item(Handle, IntPtr.Zero, label_handle);
                    else
                        ige_mac_menu_add_app_menu_item(Handle, menu_item.Handle, label_handle);
                } 
                finally
                {
                    GLib.Marshaller.Free(label_handle);
                }
            }
        }
        
        [DllImport("libigemacintegration.dylib")]
        private static extern void ige_mac_menu_connect_window_key_handler(IntPtr window);

        private static void ConnectWindowKeyHandler(Window window)
        {
            ige_mac_menu_connect_window_key_handler(window.Handle);
        }

        [DllImport("libigemacintegration.dylib")]
        private static extern void ige_mac_menu_set_global_key_handler_enabled(bool enable);

        private static bool GlobalKeyHandlerEnabled
        {
            set { ige_mac_menu_set_global_key_handler_enabled(value); }
        }

        [DllImport("libigemacintegration.dylib")]
        static extern void ige_mac_menu_set_menu_bar(IntPtr menu_shell);

        private static MenuShell MenuBar 
        {
            set 
            { 
                if (value == null)
                    ige_mac_menu_set_menu_bar(IntPtr.Zero);
                else
                    ige_mac_menu_set_menu_bar(value.Handle); 
            }
        }

        [DllImport("libigemacintegration.dylib")]
        private static extern void ige_mac_menu_set_quit_menu_item(IntPtr quit_item);

        private static MenuItem QuitMenuItem
        {
            set
            { 
                if (value == null)
                    ige_mac_menu_set_quit_menu_item(IntPtr.Zero);
                else
                    ige_mac_menu_set_quit_menu_item(value.Handle); 
            }
        }

        [DllImport ("libigemacintegration.dylib")]
        private static extern IntPtr ige_mac_menu_add_app_menu_group ();

        private static MacMenuGroup AddAppMenuGroup()
        {
            IntPtr handle = ige_mac_menu_add_app_menu_group();
            if (handle == IntPtr.Zero)
                return null;
            else
                return (MacMenuGroup)GLib.Opaque.GetOpaque(handle, typeof(MacMenuGroup), false);
        }
        
        public static void InstallGlobalMenu(MenuBar menubar, MenuItem quit_item)
        {
            MacIntegration.GlobalKeyHandlerEnabled = true;
            MacIntegration.MenuBar = menubar;
            MacIntegration.QuitMenuItem = quit_item;
            MacMenuGroup menu_group = MacIntegration.AddAppMenuGroup();
            menu_group.AddMenuItem(quit_item, null);
            menubar.Hide();
            menubar.NoShowAll = true;
        }
    }
}