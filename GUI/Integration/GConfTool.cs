﻿// GConfTool.cs created with MonoDevelop
// User: klose at 10:29 14.01.2011
// CVS release: $Id: GConfTool.cs,v 1.1 2011-04-07 09:33:57 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System.Diagnostics;


namespace Hiperscan.QuickStep.Integration
{

    public static class GConfTool
    {
        
        public static string Get(string key)
        {
            using (Process gconf_tool2 = new Process())
            {
                gconf_tool2.StartInfo.FileName = "/usr/bin/gconftool-2";
                gconf_tool2.StartInfo.Arguments = $"--get {key}";
                gconf_tool2.StartInfo.CreateNoWindow  = true;
                gconf_tool2.StartInfo.UseShellExecute = false;
                gconf_tool2.StartInfo.RedirectStandardOutput = true;
                gconf_tool2.EnableRaisingEvents = false;
                gconf_tool2.Start();

                return gconf_tool2.StandardOutput.ReadLine();
            }
        }
    }
}