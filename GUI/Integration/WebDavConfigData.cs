﻿// WebDavConfig.cs created with MonoDevelop
// User: klose at 17:32 04.08.2012
// CVS release: $Id$
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;


namespace Hiperscan.QuickStep.Integration
{

    [Serializable()]
    public class WebDAVConfigData
    {
        public DateTime     Timestamp { get; set; }
        public List<string> Files     { get; set; }
        public List<string> Contents  { get; set; }
        
        public WebDAVConfigData()
        {
            Timestamp = DateTime.Now;
            Files = new List<string>();
            Contents = new List<string>();
        }
        
        public static WebDAVConfigData ReadFromFile(string fname)
        {
            using (Stream stream = new FileStream(fname, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WebDAVConfigData));
                    WebDAVConfigData config  = (WebDAVConfigData)serializer.Deserialize(reader);
                    
                    if (config == null)
                        throw new Exception("Error while deserialization.");
                    
                    return config;
                }
            }
        }

        public void WriteToFile(string fname)
        {
            using (Stream stream = new FileStream(fname, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(stream, System.Text.Encoding.UTF8))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WebDAVConfigData));
                    serializer.Serialize(writer, this);
                }
            }
        }
    }
}