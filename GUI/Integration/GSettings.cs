﻿// GConfTool.cs created with MonoDevelop
// User: klose at 10:29 14.01.2011
// CVS release: $Id: GConfTool.cs,v 1.1 2011-04-07 09:33:57 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Diagnostics;
using Hiperscan.Unix;

namespace Hiperscan.QuickStep.Integration
{

    public static class GSettings
    {
        
        public static string Run(string command)
        {
            using (System.Diagnostics.Process gsettings = new System.Diagnostics.Process())
            {
                gsettings.StartInfo.FileName = "/usr/bin/gsettings";
                gsettings.StartInfo.Arguments = command;
                gsettings.StartInfo.CreateNoWindow  = true;
                gsettings.StartInfo.UseShellExecute = false;
                gsettings.StartInfo.RedirectStandardOutput = true;
                gsettings.EnableRaisingEvents = false;
                gsettings.Start();

                gsettings.WaitForExit();

                if (gsettings.ExitCode != 0)
                    throw new Exception(string.Format(Catalog.GetString("gsettings returned {0}."), gsettings.ExitCode));

                return gsettings.StandardOutput.ReadLine();
            }
        }

        public static string Get(string key)
        {
            return GSettings.Run($"get org.gnome.desktop.interface {key}");
        }
    }
}