﻿// MailClient.cs created with MonoDevelop
// User: klose at 09:37 14.01.2011
// CVS release: $Id: MailClient.cs,v 1.1 2011-04-07 09:33:57 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Text.RegularExpressions;

using Hiperscan.Unix;

using Microsoft.Win32;


namespace Hiperscan.QuickStep.Integration
{

    public static class MailClient
    {
        
        private enum ClientType
        {
            Evolution,
            Thunderbird,
            Outlook,
            WinMail,
            Unknown
        }
        
        private class ClientInfo
        {
            public ClientInfo(string path)
            {
                this.Path = path;
                this.ClientType = ClientType.Unknown;
                
                if (path.ToLower().Contains("thunderbird"))
                    this.ClientType = ClientType.Thunderbird;
                
                if (path.ToLower().Contains("evolution"))
                    this.ClientType = ClientType.Evolution;

                if (path.ToLower().Contains("outlook"))
                    this.ClientType = ClientType.Outlook;
                
                if (path.ToLower().Contains("winmail.exe"))
                    this.ClientType = ClientType.WinMail;
            }
            
            public void Start(string args)
            {
                Console.WriteLine("Starting mail client...");

                System.Diagnostics.Process mail_client = new System.Diagnostics.Process();
                
                mail_client.StartInfo.FileName = this.Path;
                
                mail_client.StartInfo.Arguments = args;
                mail_client.StartInfo.CreateNoWindow  = true;
                mail_client.StartInfo.UseShellExecute = false;
                mail_client.StartInfo.RedirectStandardOutput = false;
                mail_client.EnableRaisingEvents = false;
                mail_client.Start();
                
                Console.WriteLine("Success.");
            }

            public ClientType ClientType { get; set; }
            public string Path           { get; set; }
        }
        
        public static void OpenWithAttachment(string attachment)
        {
            ClientInfo info = MailClient.GetClientInfo();
            
            switch (info.ClientType)
            {
                
            case ClientType.Evolution:
                info.Start($"mailto:?attach=\"{attachment}\"");
                break;
                
            case ClientType.Thunderbird:
                info.Start($"-compose \"attachment='{attachment}'\"");
                break;
                
            case ClientType.Outlook:
                info.Start($"/a \"{attachment}\"");
                break;
                
            default:
            case ClientType.WinMail:
                throw new Exception(Catalog.GetString("The default E-mail client is unknown or not supported."));
                
            }
        }
        
        private static ClientInfo GetClientInfo()
        {
            string path = string.Empty;
            
            if (MainClass.OS == MainClass.OSType.Windows)
            {
                RegistryKey rkey = Registry.ClassesRoot.OpenSubKey("mailto\\shell\\open\\command");
                path = (string)rkey.GetValue(string.Empty);
                
                Regex regex = new Regex("\"");
                string[] s = regex.Split(path);
                
                if (s.Length < 2)
                    path = string.Empty;
                else
                    path = s[1];
            }
            else
            {
                try
                {
                    path = GConfTool.Get("/desktop/gnome/url-handlers/mailto/command");
                    if (string.IsNullOrEmpty(path))
                        path = GConfTool.Get("/desktop/gnome/applications/url-handlers/mailto/command");
                    
                    Regex regex = new Regex(" ");
                    string[] s = regex.Split(path);
                    
                    if (s.Length < 1)
                        path = string.Empty;
                    else
                        path = s[0];
                }
                catch 
                {
                    path = string.Empty;
                }
            }
            
            Console.WriteLine($"Found mail client: {path}");
            return new ClientInfo(path);
        }
    }
}