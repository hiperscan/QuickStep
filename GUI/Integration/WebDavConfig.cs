﻿// WebDavConfig.cs created with MonoDevelop
// User: klose at 17:32 04.08.2012
// CVS release: $Id$
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;


namespace Hiperscan.QuickStep.Integration
{

    public static class WebDavConfig
    {
        public static readonly string Server   = "webdav.hiperscan.com";
        public static readonly string User     = "quickstep";
        public static readonly string Pass     = "w68VQtJj";
        public static readonly string BasePath = "/home/quickstep/";
        
        public static void Read()
        {
            // FIXME: disabled due to problems under Mono 4.x
            return;

            if (!Ping())
                return;

            Console.WriteLine("WebDAV server reachable: {0}", Server);

            WebDAVClient c = new WebDAVClient
            {
                Server = $"http://{Server}",
                User = User,
                Pass = Pass,
                BasePath = BasePath
            };

            List<string> files = null;
            var auto_reset_event = new AutoResetEvent(false);
            c.ListComplete += (_files, _status) =>
            {
                files = _files;
                auto_reset_event.Set();
            };
            c.DownloadComplete += statusCode => auto_reset_event.Set();

            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                if (e.ExceptionObject is System.Net.WebException)
                {
                    Console.WriteLine(e.ExceptionObject.ToString());
                    auto_reset_event.Set();
                }
            };

            c.List();
            auto_reset_event.WaitOne();
            
            Console.WriteLine("Synchronize configuration...");

            if (Directory.Exists(UserInterface.ConfigDirPath) == false)
                Directory.CreateDirectory(UserInterface.ConfigDirPath);

            DateTime timestamp = DateTime.MinValue;
            if (File.Exists(Path.Combine(UserInterface.ConfigDirPath, 
                                         UserInterface.USER_CONFIG_FILE_NAME)))
                timestamp = File.GetLastAccessTime(Path.Combine(UserInterface.ConfigDirPath,
                                                                UserInterface.USER_CONFIG_FILE_NAME));

            foreach (string file in files)
            {
                if (file == $"{Environment.UserName}.xml")
                {
                    string tmp_file = Path.GetTempFileName();
                    auto_reset_event.Reset();
                    c.Download(file, tmp_file);
                    auto_reset_event.WaitOne();
                    WebDAVConfigData data = WebDAVConfigData.ReadFromFile(tmp_file);
                    File.Delete(tmp_file);
                    
                    if (data.Timestamp <= timestamp)
                    {
                        Console.WriteLine("Local config is more up to date.");
                        return;
                    }
                    
                    for (int ix=0; ix < data.Files.Count; ++ix)
                    {
                        if (Path.GetExtension(data.Files[ix]) == ".config")
                            continue;
                        File.WriteAllText(Path.Combine(UserInterface.ConfigDirPath,
                                                       data.Files[ix]),
                                          data.Contents[ix]);
                    }
                    break;
                }
            }
            
            Console.WriteLine("Done.");
        }
        
        public static void Write()
        {
            // FIXME: disabled due to problems under Mono 4.x
            return;

            if (WebDavConfig.Ping() == false)
                return;
            
            Console.WriteLine("WebDAV server reachable: {0}", Server);

            WebDAVClient c = new WebDAVClient
            {
                Server = $"http://{Server}",
                User = User,
                Pass = Pass,
                BasePath = BasePath
            };

            AutoResetEvent auto_reset_event = new AutoResetEvent(false);
            c.UploadComplete += (statusCode, state) => auto_reset_event.Set();
            
            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                if (e.ExceptionObject is System.Net.WebException)
                {
                    Console.WriteLine(e.ExceptionObject.ToString());
                    auto_reset_event.Set();
                }
            };

            File.WriteAllText(Path.Combine(UserInterface.ConfigDirPath, 
                                           "timestamp"), 
                              DateTime.Now.Ticks.ToString());

            Console.WriteLine("Synchronize configuration...");

            WebDAVConfigData data = new WebDAVConfigData();
            foreach (string file in Directory.GetFiles(UserInterface.ConfigDirPath))
            {
                if (Path.GetExtension(file) == ".config")
                    continue;
                data.Files.Add(Path.GetFileName(file));
                data.Contents.Add(File.ReadAllText(file));
            }
            
            string tmp_file = Path.GetTempFileName();
            data.WriteToFile(tmp_file);

            c.Upload(tmp_file, $"{Environment.UserName}.xml");
            auto_reset_event.WaitOne(1000);
            File.Delete(tmp_file);
            
            Console.WriteLine("Done.");
        }
        
        private static bool Ping()
        {
            return Integration.Ping.Send(Server);
        }
    }
}