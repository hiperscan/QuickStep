#!/bin/bash
# initialize target directory (plugin, internationalization, themes)
# User: klose at 16:54 09.01.2012

PROFILE=`basename $PWD`

rm -rf Plugins
rm -rf Themes
mkdir -p Plugins

if [ -x $PWD/../../../../HCL/MDnsSearch ]
then
  echo Found MDnsSearch
  ln -sf $PWD/../../../../HCL/MDnsSearch/bin/$PROFILE/MDnsSearch.exe .
fi

if [ -x $PWD/../../../../ApoIdent ]
then
  echo Found Plugin: ApoIdent
  ln -sf $PWD/../../../../ApoIdent/ApoIdent/bin/$PROFILE Plugins/ApoIdent
fi


if [ -x ../../../../Plugins ]
then
  for PROJECT in `find ../../../../Plugins/ -name '*.csproj'`
  do
    PLUGIN=`basename $PROJECT .csproj`
    if [ -d $PWD/../../../../Plugins/$PLUGIN/bin/$PROFILE ]
    then
      echo Found Plugin: $PLUGIN
	  ln -sf $PWD/../../../../Plugins/$PLUGIN/bin/$PROFILE Plugins/$PLUGIN
	fi
  done
fi

if [ -x ../../../../Pluginscs ]
then
  for PROJECT in `find ../../../../Pluginscs/ -name '*.csproj'`
  do
    PLUGIN=`basename $PROJECT .csproj`
    if [ -d $PWD/../../../../Pluginscs/$PLUGIN/bin/$PROFILE ]
    then
      echo Found Plugin: $PLUGIN
	  ln -sf $PWD/../../../../Pluginscs/$PLUGIN/bin/$PROFILE Plugins/$PLUGIN
	fi
  done
fi

if [ -x ../../../../Internationalization/Internationalization ]
then
  for PO in `find ../../../../Internationalization/Internationalization -name '??.po'`
  do
    LOCALIZATION=`basename $PO .po`
    echo Found localization: $LOCALIZATION
    mkdir -p $LOCALIZATION/LC_MESSAGES
    msgfmt -o $LOCALIZATION/LC_MESSAGES/Hiperscan.mo $PO
  done
fi

if [ -x ../../../Installer/Themes ]
then
  ln -sf ../../../Installer/Themes
fi
