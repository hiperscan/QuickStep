﻿// ZoomBar.cs created with MonoDevelop
// User: klose at 13:48 16.12.2008
// CVS release: $Id: ZoomBar.cs,v 1.14 2010-11-25 13:52:56 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Gtk;


namespace Hiperscan.QuickStep.Zoombar
{

    internal class Handle : Hiperscan.QuickStep.Zoombar.ZoomBarButton
    {
        public Gdk.ModifierType ModifierState; 
        
        public delegate void DoubleClickedHandler(Handle h);
        public event         DoubleClickedHandler DoubleClicked;
                
        
        public Handle() : base()
        {
            this.Relief = ReliefStyle.Normal;
        }
        
        protected override bool OnButtonPressEvent(Gdk.EventButton e)
        {
            this.ModifierState = e.State;
            
            if ((e.Button == 1) && (e.Type == Gdk.EventType.TwoButtonPress))
            {
                this.DoubleClicked?.Invoke(this);

                return true;
            }
            
            return base.OnButtonPressEvent(e);
        }        
    }
}