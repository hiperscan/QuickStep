﻿// ZoomBar.cs created with MonoDevelop
// User: klose at 13:48 16.12.2008
// CVS release: $Id: ZoomBar.cs,v 1.14 2010-11-25 13:52:56 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Gtk;


namespace Hiperscan.QuickStep.Zoombar
{

    public class VZoombar : Hiperscan.QuickStep.Zoombar.Zoombar
    {

        protected static Style scrollbar_style;

        public VZoombar() : base(0f, 0f, 0f, 1f)
        {
            this.Name = "Vertical Zoom Bar";
            
            Arrow arrow;
            VBox  vb;

            // center handle
            Alignment align = new Alignment(0.5f, 0.5f, 0.7f, 1.0f); 
            vb = new VBox();
            VBox vb2 = new VBox();
            vb.Spacing = 3;
            vb2.PackStart(vb, true, false, 0);
            
            for (int ix=1; ix < 4; ++ix)
            {
                vb.PackStart(new HSlideSeparator());
            }
            align.Add(vb2);
            this.buttons[2].Add(align);

            // up handle
            vb = new VBox();
            arrow = new Arrow(ArrowType.Up, ShadowType.In);
            vb.PackStart(arrow, true, true, 0);
            arrow = new Arrow(ArrowType.Down, ShadowType.In);
            vb.PackStart(arrow, true, true, 0);
            this.buttons[3].Add(vb);

            // down handle
            vb = new VBox();
            arrow = new Arrow(ArrowType.Up, ShadowType.In);
            vb.PackStart(arrow, true, true, 0);
            arrow = new Arrow(ArrowType.Down, ShadowType.In);
            vb.PackStart(arrow, true, true, 0);
            this.buttons[4].Add(vb);

            // scroll buttons
            arrow = new Arrow(ArrowType.Up, ShadowType.In);
            this.buttons[5].Add(arrow);
            this.buttons[5].Name = "Up";
            
            arrow = new Arrow(ArrowType.Down, ShadowType.In);
            this.buttons[6].Add(arrow);
            this.buttons[6].Name = "Down";
            
            this.frame.ScrollEvent += this.OnScrollEvent;

            this.ModifyStyle();
            this.StyleSet += (o, args) =>
            {
                VZoombar.scrollbar_style = null;
                this.ModifyStyle();
            };
            
            // update geometry
            this.Update();
        }
        
        private void ModifyStyle()
        {
            Style style = this.GetScrollbarStyle();
            if (style == null)
                return;

            foreach (ZoomBarButton b in this.buttons)
            {
                b.Style = style;
            }
        }
        
        private Style GetScrollbarStyle()
        {
            if (VZoombar.scrollbar_style == null)
            {
                VScrollbar sb = new VScrollbar(new Adjustment(0f, 1f, 1f, 0.1f, 0.2f, 0f));
                VZoombar.scrollbar_style = Rc.GetStyle(sb);
                int w = sb.SizeRequest().Width;
                
                if (w > 4)
                    this.width = (w % 2 == 1) ? w : w+1;
                
                if (Settings.Default.ThemeName == "New Wave")
                    VZoombar.scrollbar_style = Rc.GetStyle(new Button());

                if (Settings.Default.ThemeName == "Adwaita")
                {
                    VZoombar.scrollbar_style = Rc.GetStyle(new Button());
                    this.width = 19;
                }
            }
            
            return VZoombar.scrollbar_style;
        }
        
        protected override void Update()
        {
            int pl  = frame_height; 
            int pw  = Width;
            
            this.CalculateGeomParams(pl, pw);
            
            if (l < 0 || w < 0 || offset < 0)
                return; // invalid data -> do nothing

            this.buttons[0].HeightRequest = (int)Math.Round(offset);
            this.buttons[0].WidthRequest  = (int)Math.Round(w);
            this.buttons[1].HeightRequest = (int)Math.Round(lmax - l - offset);
            this.buttons[1].WidthRequest  = (int)Math.Round(w);
            this.buttons[2].HeightRequest = (int)Math.Round(l); // - 2*Defaults.HandleLength);
            this.buttons[2].WidthRequest  = (int)Math.Round(w);
            this.buttons[3].HeightRequest = (int)Math.Round(Defaults.HandleLength);
            this.buttons[3].WidthRequest  = (int)Math.Round(w);
            this.buttons[4].HeightRequest = (int)Math.Round(Defaults.HandleLength);
            this.buttons[4].WidthRequest  = (int)Math.Round(w);
            this.buttons[5].HeightRequest = (int)Math.Round(Defaults.ButtonLength);
            this.buttons[5].WidthRequest  = (int)Math.Round(w);
            this.buttons[6].HeightRequest = (int)Math.Round(Defaults.ButtonLength);
            this.buttons[6].WidthRequest  = (int)Math.Round(w);
    
            this.Move(this.buttons[0], 0, (int)Math.Round(Defaults.ButtonLength));
            this.Move(this.buttons[1], 0, (int)Math.Round(Defaults.ButtonLength + offset + l));
            this.Move(this.buttons[2], 0, (int)Math.Round(Defaults.ButtonLength + offset)); // + Defaults.HandleLength));
            this.Move(this.buttons[3], 0, (int)Math.Round(Defaults.ButtonLength + offset));
            this.Move(this.buttons[4], 0, (int)Math.Round(Defaults.ButtonLength + l + offset - Defaults.HandleLength));
            this.Move(this.buttons[5], 0, 0);
            this.Move(this.buttons[6], 0, (int)Math.Round(Defaults.ButtonLength + lmax));
            
            if (Math.Abs(zoom - 1.0) < EPSILON || Math.Abs(pan) < EPSILON)
                this.buttons[5].Sensitive = false;
            else
                 this.buttons[5].Sensitive = true;
                        
            if (Math.Abs(zoom - 1.0) < EPSILON || Math.Abs(pan - 1.0) < EPSILON)
                this.buttons[6].Sensitive = false;
            else
                this.buttons[6].Sensitive = true;

            this.SetSizeRequest(pw, (int)(lmin + 2*Defaults.ButtonLength + 10));

            // execute event handlers
            this.ChangeNotify(this.dragging == false);
        }
        
        private void OnScrollEvent(object o, ScrollEventArgs e)
        {
            double pan_step = scroll_step/(zoom-1);

            if (e.Event.Direction == Gdk.ScrollDirection.Down)
            {
                if (pan + pan_step <= 1.0)
                    pan += pan_step;
                else
                    pan = 1.0;
            }
            else
            {
                if (pan - pan_step >= 0.0)
                    pan -= pan_step;
                else
                    pan = 0.0;
            }            
            this.Update();
        }    
    }        
}