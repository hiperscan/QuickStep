#!/bin/sh

set -e

CDIR=$(pwd)

if [ -z $1 ]
then
  echo "Cannot run prebuild scripts without a Profile."
  exit 1
fi

for SCRIPT in $(find .. -type f -name 'prebuild.sh')
do
  echo "Running prebuild script $SCRIPT..."
  cd $(dirname $SCRIPT)
  bash prebuild.sh
  cd $CDIR
done
